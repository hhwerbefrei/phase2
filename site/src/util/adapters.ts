export function rewrite_wpsite_to_next(newSiteBase: URL, link?: string | null) {
  if (!link) return '';

  // link is relative
  if (/^[/]/.test(link)) {
    if (redirectSpecialRoutes.has(link)) {
      return redirectSpecialRoutes.get(link);
    }
    return link;
  }

  let parsedLink;
  try {
    parsedLink = new URL(link);
  } catch (e) {
    console.error(e);
  }

  // console.log(newSiteBase);

  const isOriginalWordpressSite = (host: string | undefined) =>
    [
      'hamburg-werbefrei.de',
      'www.hamburg-werbefrei.de',
      'wp.hamburg-werbefrei.de',
      undefined,
    ].includes(host);

  // artifact of the query, it must've been a relative link
  if (
    isOriginalWordpressSite(parsedLink?.host) &&
    parsedLink?.pathname.startsWith('/graphql')
  ) {
    return /[/]graphql[/](.*)/.exec(parsedLink.pathname)?.at(1)!;
  }
  if (
    isOriginalWordpressSite(parsedLink?.host) &&
    parsedLink?.pathname === '/' &&
    /[?](?:p|page_id)=\d+/.test(parsedLink.search)
  ) {
    const match = /[?](p|page_id)=(\d+)/.exec(parsedLink.search);
    const resourceType = match?.at(1) === 'p' ? 'post' : 'page';
    return `${newSiteBase.toString()}wp/${resourceType}/${match?.at(2)}`;
  }
  if (parsedLink?.host === 'phase2.hamburg-werbefrei.de') {
    if (parsedLink.pathname === '/') return `${newSiteBase.toString()}sammeln`;
    if (parsedLink.pathname === '/briefeintragung')
      return `${newSiteBase.toString()}briefeintragung`;
    return link;
  }
  if (!!parsedLink && isOriginalWordpressSite(parsedLink?.host)) {
    parsedLink.host = newSiteBase.host;
    parsedLink.protocol = newSiteBase.protocol;

    if (parsedLink.pathname === '/') {
      return newSiteBase.toString() + parsedLink.hash;
    }

    // console.log(parsedLink.pathname, pageMap.has(parsedLink.pathname));

    if (pageMap.has(parsedLink.pathname)) {
      parsedLink.pathname = makeNewSiteWpPagePath(
        pageMap.get(parsedLink.pathname)!,
      );
    }

    if (redirectSpecialRoutes.has(parsedLink.pathname)) {
      parsedLink.pathname = redirectSpecialRoutes.get(parsedLink.pathname)!;
    }

    return parsedLink.toString();
  }
  return link;
}

function makeNewSiteWpPagePath(page_id: number): string {
  return `/wp/page/${page_id}`;
}

import { pageMap, redirectSpecialRoutes } from 'data/pagemap';
import { HTMLRewriter } from 'htmlrewriter';

export const rewriter = (newSiteBase: URL) =>
  new HTMLRewriter()
    .on('a', {
      element(a) {
        const gotten = a.getAttribute('href');
        const rewritten = rewrite_wpsite_to_next(newSiteBase, gotten);
        console.debug(gotten, '->', rewritten);
        a.setAttribute('href', rewritten ?? '');
      },
    })
    // Disabled, because the "gutenberg block" links don't work here
    // Will have to build a replacement (can't be too hard. get list
    // of excerpts and style in the same manner)
    // TODO
    .on('div.post-grid.grid .pagination', {
      element(div) {
        div.setAttribute('style', 'display:none;');
      },
    });

// leave images as they are, they work and will continue to work.

//   .on('img', {
//     element(img) {
//       img.setAttribute(
//         'src',
//         rewrite_www_to_next(img.getAttribute('src') ?? undefined) ?? '',
//       );
//       img.setAttribute(
//         'srcset',
//         rewrite_www_to_next_multiple(img.getAttribute('src') ?? undefined) ??
//           '',
//       );
//     },
// });
