import { rewrite_wpsite_to_next } from '@util/adapters';

export { getMenuData, menuItemHasChildren };

const menu_data_raw = [
  {
    db_id: 5880,
    type: 'custom',
    title: 'Start',
    link: 'https:\/\/www.hamburg-werbefrei.de\/?reload=#start',
    position: 1,
    menu_item_parent: '0',
  },
  {
    db_id: 4414,
    type: 'custom',
    title: 'News',
    link: '/news',
    position: 2,
    menu_item_parent: '0',
  },
  {
    db_id: 4517,
    type: 'post_type',
    title: 'Gesetzentwurf',
    link: 'https:\/\/www.hamburg-werbefrei.de\/gesetzentwurf\/',
    position: 3,
    menu_item_parent: '5628',
  },
  {
    db_id: 5628,
    type: 'custom',
    title: 'Volksbegehren',
    link: 'https:\/\/phase2.hamburg-werbefrei.de\/briefeintragung',
    position: 4,
    menu_item_parent: '0',
  },
  {
    db_id: 4525,
    type: 'post_type',
    title: 'Vorher \/ Nachher',
    link: 'https:\/\/www.hamburg-werbefrei.de\/gruende\/vorher-nachher\/',
    position: 5,
    menu_item_parent: '0',
  },
  {
    db_id: 4224,
    type: 'taxonomy',
    title: 'Mitmachen',
    link: 'https:\/\/www.hamburg-werbefrei.de\/category\/mitmachen\/',
    position: 6,
    menu_item_parent: '0',
  },
  {
    db_id: 5362,
    type: 'custom',
    title: 'Registrieren als Sammler:in',
    link: 'https:\/\/phase2.hamburg-werbefrei.de\/',
    position: 7,
    menu_item_parent: '4224',
  },
  {
    db_id: 4595,
    type: 'post_type',
    title: 'Material',
    link: 'https:\/\/www.hamburg-werbefrei.de\/gesetzentwurf\/',
    position: 8,
    menu_item_parent: '0',
  },
  {
    db_id: 5833,
    type: 'post_type',
    title: 'Flyer, Sticker, Poster',
    link: 'https:\/\/www.hamburg-werbefrei.de\/uncategorized\/flyer-sticker-poster\/',
    position: 9,
    menu_item_parent: '4595',
  },
  {
    db_id: 4940,
    type: 'post_type',
    title: 'Druckvorlage Flyer und SharePic',
    link: 'https:\/\/www.hamburg-werbefrei.de\/druckvorlage-flyer\/',
    position: 10,
    menu_item_parent: '4595',
  },
  {
    db_id: 4650,
    type: 'post_type',
    title: 'Pressebereich',
    link: 'https:\/\/www.hamburg-werbefrei.de\/uncategorized\/presse\/',
    position: 11,
    menu_item_parent: '4595',
  },
  {
    db_id: 5044,
    type: 'post_type',
    title: 'Spenden',
    link: 'https:\/\/www.hamburg-werbefrei.de\/spenden\/',
    position: 12,
    menu_item_parent: '0',
  },
  {
    db_id: 4735,
    type: 'custom',
    title: 'Termine',
    link: 'https:\/\/www.hamburg-werbefrei.de\/uncategorized\/termine\/',
    position: 13,
    menu_item_parent: '0',
  },
  {
    db_id: 3856,
    type: 'post_type',
    title: 'Kontakt',
    link: 'https:\/\/www.hamburg-werbefrei.de\/impressum-2\/',
    position: 14,
    menu_item_parent: '0',
  },
  {
    db_id: 4402,
    type: 'custom',
    title: '\u00dcber uns',
    link: 'https:\/\/www.hamburg-werbefrei.de\/#ueberuns',
    position: 15,
    menu_item_parent: '3856',
  },
  {
    db_id: 5845,
    type: 'post_type',
    title: 'Impressum',
    link: 'https:\/\/www.hamburg-werbefrei.de\/impressum-2\/',
    position: 16,
    menu_item_parent: '3856',
  },
  {
    db_id: 3896,
    type: 'post_type',
    title: 'Datenschutzerkl\u00e4rung',
    link: 'https:\/\/www.hamburg-werbefrei.de\/privacy-policy-2\/',
    position: 17,
    menu_item_parent: '3856',
  },
];

const getMenuData = (newSiteBase: URL) =>
  menu_data_raw.map((item) => ({
    ...item,
    link: rewrite_wpsite_to_next(newSiteBase, item.link) ?? item.link,
  }));

function menuItemHasChildren(
  the_menu_data: typeof menu_data_raw,
  db_id: number,
) {
  return (
    the_menu_data.filter(
      (inner_item) => inner_item.menu_item_parent === db_id.toString(),
    ).length > 0
  );
}
