export { pageMap, redirectSpecialRoutes };

// Suppress some pages when they crop up, because they've been replaced with Astro-"native" components; also, some top-level links
const redirectSpecialRoutes = new Map([
  ['/wp/post/4023', '/vorher-nachher'],
  ['/spenden', '/wp/page/4763'],
  ['/wp/page/5825', '/uncategorized/flyer-sticker-poster'],
  ['/wp/page/3837', '/newsletter'],
  ['/flyer-sticker-poster', '/uncategorized/flyer-sticker-poster'],
]);

const page_data_raw = [
  { ID: 5951, url: 'https://www.hamburg-werbefrei.de/?page_id=5951' },
  { ID: 5946, url: 'https://www.hamburg-werbefrei.de/zitate-2/' },
  { ID: 5938, url: 'https://www.hamburg-werbefrei.de/zitate/' },
  { ID: 5933, url: 'https://www.hamburg-werbefrei.de/newsletter-23/' },
  { ID: 5922, url: 'https://www.hamburg-werbefrei.de/abgeordnetenwatch/' },
  { ID: 5882, url: 'https://www.hamburg-werbefrei.de/?page_id=5882' },
  { ID: 5837, url: 'https://www.hamburg-werbefrei.de/newsletter-22/' },
  {
    ID: 5825,
    url: 'https://www.hamburg-werbefrei.de/flyer-sticker-poster/',
  },
  { ID: 5802, url: 'https://www.hamburg-werbefrei.de/signalgruppen/' },
  { ID: 5793, url: 'https://www.hamburg-werbefrei.de/newsletter-21/' },
  { ID: 5744, url: 'https://www.hamburg-werbefrei.de/?p=5744' },
  { ID: 5752, url: 'https://www.hamburg-werbefrei.de/?p=5752' },
  { ID: 5704, url: 'https://www.hamburg-werbefrei.de/newsletter-20/' },
  {
    ID: 5601,
    url: 'https://www.hamburg-werbefrei.de/pressemitteilung-der-volksinitiative-hamburg-werbefrei-ein-sieg-fuer-die-demokratie-und-fuer-die-gesamte-stadt/',
  },
  {
    ID: 5572,
    url: 'https://www.hamburg-werbefrei.de/warten-auf-urteil-am-6-9-24/',
  },
  { ID: 5539, url: 'https://www.hamburg-werbefrei.de/karte/' },
  {
    ID: 5527,
    url: 'https://www.hamburg-werbefrei.de/kunst-statt-werbung/',
  },
  {
    ID: 5519,
    url: 'https://www.hamburg-werbefrei.de/endlich-ein-termin-fuer-die-muendliche-verhandlung-beim-landesverfassungericht/',
  },
  {
    ID: 5488,
    url: 'https://www.hamburg-werbefrei.de/attac-hamburg-unterstuetzt-hamburg-werbefrei/',
  },
  {
    ID: 5459,
    url: 'https://www.hamburg-werbefrei.de/senat-verklagt-hamburg-werbefrei-vor-dem-verfassungsgericht-scharfe-kritik-an-spd-und-gruenen/',
  },
  {
    ID: 5412,
    url: 'https://www.hamburg-werbefrei.de/teilnahme-an-fallschirmaktion-von-parents-for-future/',
  },
  {
    ID: 5343,
    url: 'https://www.hamburg-werbefrei.de/senat-bestaetigt-erfolg-von-hamburg-werbefrei/',
  },
  {
    ID: 5291,
    url: 'https://www.hamburg-werbefrei.de/volksinitiative-hamburg-werbefrei-erfolgreich-forderung-nach-sofortigem-werbestopp/',
  },
  {
    ID: 5230,
    url: 'https://www.hamburg-werbefrei.de/sammeldemo-am-15-10/',
  },
  {
    ID: 5086,
    url: 'https://www.hamburg-werbefrei.de/werbeindustrie-raeumt-hohen-energieverbrauch-ein-politik-muss-sofort-handeln/',
  },
  {
    ID: 5080,
    url: 'https://www.hamburg-werbefrei.de/waermebilder-beweisen-werbemonitore-verschwenden-energie-und-heizen-staedte-auf/',
  },
  { ID: 5039, url: 'https://www.hamburg-werbefrei.de/spenden-2/' },
  {
    ID: 5029,
    url: 'https://www.hamburg-werbefrei.de/bergfest-5000-unterschrift-und-verhuellung-eines-werbemonitors/',
  },
  {
    ID: 5008,
    url: 'https://www.hamburg-werbefrei.de/diese-organisationen-unterstuetzen-hamburg-werbefrei/',
  },
  {
    ID: 4968,
    url: 'https://www.hamburg-werbefrei.de/halbzeit-wir-koennen-es-schaffen/',
  },
  { ID: 4937, url: 'https://www.hamburg-werbefrei.de/druckvorlage-flyer/' },
  { ID: 4763, url: 'https://www.hamburg-werbefrei.de/spenden/' },
  { ID: 4751, url: 'https://www.hamburg-werbefrei.de/sammelfortschritt/' },
  { ID: 4731, url: 'https://www.hamburg-werbefrei.de/termine/' },
  {
    ID: 4726,
    url: 'https://www.hamburg-werbefrei.de/9-6-podiumsdiskussion-an-der-tuhh/',
  },
  {
    ID: 4679,
    url: 'https://www.hamburg-werbefrei.de/11-6-um-15-uhr-offenes-treffen-fuer-interessierte-centro-sociale/',
  },
  {
    ID: 4674,
    url: 'https://www.hamburg-werbefrei.de/25-5-19-uhr-werbefreie-kneipe-jupi-bar/',
  },
  { ID: 4654, url: 'https://www.hamburg-werbefrei.de/sammelorte-karte/' },
  { ID: 4618, url: 'https://www.hamburg-werbefrei.de/?p=4618' },
  { ID: 4606, url: 'https://www.hamburg-werbefrei.de/presse/' },
  {
    ID: 4552,
    url: 'https://www.hamburg-werbefrei.de/bilder-vom-sammelstart/',
  },
  {
    ID: 4541,
    url: 'https://www.hamburg-werbefrei.de/jetzt-unterschreiben/',
  },
  {
    ID: 4532,
    url: 'https://www.hamburg-werbefrei.de/ausfuehrliches-radiofeature-bei-deutschlandfunk/',
  },
  {
    ID: 4530,
    url: 'https://www.hamburg-werbefrei.de/montag-25-4-sammelstart-vor-dem-rathaus/',
  },
  {
    ID: 4522,
    url: 'https://www.hamburg-werbefrei.de/3-treffen-fuer-interessierte-am-30-04-2022-samstag-1500-1700/',
  },
  { ID: 4511, url: 'https://www.hamburg-werbefrei.de/gesetzentwurf/' },
  {
    ID: 4508,
    url: 'https://www.hamburg-werbefrei.de/beitrag-bei-hamburg1/',
  },
  {
    ID: 4423,
    url: 'https://www.hamburg-werbefrei.de/kleine-verzoegerung/',
  },
  { ID: 4325, url: 'https://www.hamburg-werbefrei.de/?page_id=4325' },
  { ID: 4355, url: 'https://www.hamburg-werbefrei.de/?page_id=4355' },
  { ID: 4284, url: 'https://www.hamburg-werbefrei.de/' },
  { ID: 4249, url: 'https://www.hamburg-werbefrei.de/4249/' },
  { ID: 4232, url: 'https://www.hamburg-werbefrei.de/ein-vorgeschmack/' },
  {
    ID: 4220,
    url: 'https://www.hamburg-werbefrei.de/2-treffen-fuer-interessierte-am-19-02-2022-samstag-1500-1700-nur-online/',
  },
  {
    ID: 4205,
    url: 'https://www.hamburg-werbefrei.de/hamburg-werbefrei-stellt-sich-vor/',
  },
  {
    ID: 4200,
    url: 'https://www.hamburg-werbefrei.de/1-treffen-fuer-interessierte/',
  },
  { ID: 4195, url: 'https://www.hamburg-werbefrei.de/?page_id=4195' },
  { ID: 4023, url: 'https://www.hamburg-werbefrei.de/vorher-nachher/' },
  {
    ID: 4040,
    url: 'https://www.hamburg-werbefrei.de/werbebelastung-in-hamburg/',
  },
  {
    ID: 3998,
    url: 'https://www.hamburg-werbefrei.de/senat-beantwortet-fragen-zur-vertragsverlaengerung-nicht/',
  },
  {
    ID: 3970,
    url: 'https://www.hamburg-werbefrei.de/pressemitteilung-zu-heimlicher-senatsentscheidung/',
  },
  {
    ID: 3941,
    url: 'https://www.hamburg-werbefrei.de/pressespiegel-12-6-2021/',
  },
  { ID: 3928, url: 'https://www.hamburg-werbefrei.de/diskussionsbeitrag/' },
  { ID: 3921, url: 'https://www.hamburg-werbefrei.de/news/' },
  { ID: 3902, url: 'https://www.hamburg-werbefrei.de/pressespiegel/' },
  { ID: 3837, url: 'https://www.hamburg-werbefrei.de/newsletter/' },
  { ID: 3561, url: 'https://www.hamburg-werbefrei.de/impressum-2/' },
  { ID: 3527, url: 'https://www.hamburg-werbefrei.de/?page_id=3527' },
  { ID: 3525, url: 'https://www.hamburg-werbefrei.de/?page_id=3525' },
  { ID: 3523, url: 'https://www.hamburg-werbefrei.de/?page_id=3523' },
  { ID: 3521, url: 'https://www.hamburg-werbefrei.de/?page_id=3521' },
  { ID: 3519, url: 'https://www.hamburg-werbefrei.de/?page_id=3519' },
  { ID: 3516, url: 'https://www.hamburg-werbefrei.de/?page_id=3516' },
  { ID: 3513, url: 'https://www.hamburg-werbefrei.de/?page_id=3513' },
  { ID: 3501, url: 'https://www.hamburg-werbefrei.de/privacy-policy-2/' },
  { ID: 3495, url: 'https://www.hamburg-werbefrei.de/?page_id=3495' },
  {
    ID: 3479,
    url: 'https://www.hamburg-werbefrei.de/warum-hamburg-werbefrei/',
  },
  { ID: 3477, url: 'https://www.hamburg-werbefrei.de/uber-uns/' },
  { ID: 3388, url: 'https://www.hamburg-werbefrei.de/impressum/' },
  { ID: 1706, url: 'https://www.hamburg-werbefrei.de/gruende/' },
  { ID: 18, url: 'https://www.hamburg-werbefrei.de/startseite/' },
];

const init: [string, number][] = page_data_raw.map(({ ID, url }) => {
  const parsedUrl = new URL(url);
  return [parsedUrl.pathname ?? url, ID];
});

const pageMap: Map<string, number> = new Map(init);
