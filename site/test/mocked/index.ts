import type { NightwatchAPI } from 'nightwatch';
import type { Request, Response } from 'express';
import express from 'express';
import type { Express } from 'express';
import type { Server, IncomingMessage, ServerResponse } from 'http';
import { scrollDownToId, scrollDownToSelector } from '../common/util';

describe('Frontpage (mocked backend)', function () {
  let app: Express;
  let server: Server<typeof IncomingMessage, typeof ServerResponse>;

  const url_base = 'http://localhost:4321';
  const start = `${url_base}/`;

  before(async (client, done) => {
    app = express();

    app.post(
      '/submit/subscribeNewsletter',
      function (req: Request, res: Response) {
        res.redirect(`${start}?success=true`);
      },
    );

    server = app.listen(3001, function () {
      done();
    });
  });

  it('check page title and basic content', () => {
    this.tags = ['desktop', 'mobile'];
    browser.navigateTo(start);
    browser.assert.titleMatches('Hamburg Werbefrei');
    browser.element.find('#wartungstext').assert.not.visible();
  });

  // TODO differentiate between desktop-only and mobile-only tests where there's a big difference.
  it('check menu present (desktop)', () => {
    this.tags = ['desktop'];
    browser.navigateTo(start);
    browser.element.find('.logo-img').assert.visible();
    browser.element.find('a[href*="impressum"]').assert.visible();
  });

  it('check newsletter subscription with mocked backend', () => {
    this.tags = ['desktop'];
    browser.navigateTo(start);
    browser.element.find('a[href="/newsletter"]').assert.present();
    scrollDownToSelector('a[href="/newsletter"]');
    browser.element.find('a[href="/newsletter"]').click();
    browser.pause(500);
    browser.assert.urlContains('/newsletter');
    browser.element.find('input#submit-abonnieren').click();
    browser.assert.urlContains('/newsletter');
    browser.element.find('input#email').sendKeys('max');
    browser.element.find('input#submit-abonnieren').click();
    browser.assert.urlContains('/newsletter');
    browser.element.find('input#datenschutz-checkbox').click();
    browser.element.find('input#submit-abonnieren').click();
    browser.assert.urlContains('/newsletter');
    browser.element.find('input#email').clear();
    browser.element.find('input#email').sendKeys('max@example.com');
    browser.element.find('input#submit-abonnieren').click();
    browser.pause(500);
    browser.assert.urlEquals(start);
    browser.element.find('.successbox-toast').assert.visible();
    browser.element.find('.successbox-toast').click();
    browser.element.find('.successbox-toast').assert.not.visible();
  });

  it('check newsletter subscription on front page with mocked backend', () => {
    this.tags = ['desktop'];
    browser.navigateTo(start);
    browser.element.find('input#email').sendKeys('max');
    browser.element.find('input#submit-abonnieren').click();
    browser.element.find('input#datenschutz-checkbox').click();
    browser.element.find('input#submit-abonnieren').click();
    browser.element.find('input#email').clear();
    browser.element.find('input#email').sendKeys('max@example.com');
    browser.element.find('input#submit-abonnieren').click();
    browser.pause(500);
    browser.assert.urlEquals(start);
    browser.element.find('.successbox-toast').assert.visible();
    browser.element.find('.successbox-toast').click();
    browser.element.find('.successbox-toast').assert.not.visible();
  });

  // TODO apply this to all subpages as well!
  it('run all accessibility rules on /', () => {
    this.tags = ['desktop', 'mobile'];
    browser.navigateTo(start);
    browser.axeInject().axeRun('body');
  });

  it('run all accessibility rules on /sammeln', () => {
    this.tags = ['desktop', 'mobile'];
    browser.navigateTo(`${url_base}/sammeln`);
    browser.axeInject().axeRun('body');
  });

  after(() => {
    browser.end();
    server.close();
  });
});
