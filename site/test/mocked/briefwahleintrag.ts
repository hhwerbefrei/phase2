import type { NightwatchAPI } from 'nightwatch';
import type { Request, Response } from 'express';
import express from 'express';
import type { Express } from 'express';
import type { Server, IncomingMessage, ServerResponse } from 'http';
import { scrollDownToId } from '../common/util';

describe('Briefeintragung page (mocked backend)', function () {
  let app: Express;
  let server: Server<typeof IncomingMessage, typeof ServerResponse>;

  const url_base = 'http://localhost:4321';
  const start = `${url_base}/briefeintragung`;
  const url_success = `${url_base}/success_briefwahl`;

  before(async (client, done) => {
    app = express();

    app.post('/briefeintragung/send', function (req: Request, res: Response) {
      res.redirect(`${url_success}?success=true`);
    });

    server = app.listen(3001, function () {
      done();
    });
  });

  it('check page title and basic content', () => {
    this.tags = ['desktop', 'mobile'];
    browser.navigateTo(start);
    browser.assert.titleMatches(
      'Hamburg Werbefrei(Lokal Test) | Briefeintragungsunterlagen anfordern',
    );
    browser.element.find('#wartungstext').assert.not.visible();
  });

  it('check form', () => {
    this.tags = ['desktop', 'mobile'];
    browser.navigateTo(start);
    const button = browser.element.find('form button');
    expect(button).text.toEqual('Absenden');

    button.assert.visible();

    browser.element.find('#vorname').sendKeys('Max');
    browser.element.find('#nachname').sendKeys('Musterfrau');
    browser.element.find('#strassenr').sendKeys('Beispielstr. 1');
    browser.element.find('#plz').sendKeys('20095');

    scrollDownToId('absenden');
    browser.element('#absenden').click();
    browser.assert.urlEquals(start);

    scrollDownToId('beantragung');
    browser.waitForElementVisible('#beantragung');
    browser.element.find('#beantragung').click();
    browser.element.find('#datenschutz').click();

    // Beantragung und Datenschutz an, aber PLZ ist nicht in Hamburg:
    browser.element.find('#plz').clear();
    browser.element.find('#plz').sendKeys('23095');

    scrollDownToId('absenden');
    browser.element('#absenden').click();
    browser.assert.urlEquals(start);

    browser.element.find('#plz').clear();
    browser.element.find('#plz').sendKeys('22527');

    scrollDownToId('absenden');
    browser.element('#absenden').click();
    browser.assert.urlEquals(url_success);

    const successMessageSpan = browser.element.find('#successmessage');
    successMessageSpan.assert.visible();
    expect(successMessageSpan).text.toEqual(
      'Die Eingaben wurden an das Hamburg Werbefrei Team gesendet.',
    );

    browser.assert.titleMatches(
      'Hamburg Werbefrei(Lokal Test) | Briefeintragungsunterlagen anfordern',
    );
  });

  it('run all accessibility rules', () => {
    this.tags = ['desktop', 'mobile'];
    browser.navigateTo(start);
    browser.axeInject().axeRun('body');
  });

  after(() => {
    browser.end();
    server.close();
  });
});
