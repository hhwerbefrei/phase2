export const scrollDownToId = (id: string) => {
  browser.execute(
    function (id) {
      document.getElementById(id)?.scrollIntoView({
        block: 'end',
        inline: 'end',
        behavior: 'instant',
      });
    },
    [id],
  );
  // experimentally determined value
  // TODO how to do this properly?
  browser.execute('scrollBy(0, 500)');
  browser.pause(100);
  browser.waitForElementVisible(`#${id}`);
};

export const scrollDownToSelector = (css: string) => {
  browser.execute(
    function (css) {
      document.querySelector(css)?.scrollIntoView({
        block: 'end',
        inline: 'end',
        behavior: 'instant',
      });
    },
    [css],
  );
  // experimentally determined value
  // TODO how to do this properly?
  browser.execute('scrollBy(0, 500)');
  browser.pause(100);
  browser.waitForElementVisible(css);
};
