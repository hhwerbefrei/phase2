import type { NightwatchAPI } from 'nightwatch';
import { scrollDownToId } from '../common/util';
import * as fs from 'fs';

const SITEMAP_FILE = 'sitemap.json';

const live_url_base = 'https://www.hamburg-werbefrei.de';
const new_url_base = 'http://localhost:4321';

const api_base_url = 'https://www.hamburg-werbefrei.de/wp-json/wp/v2';

const runId = Date.now();

describe('Briefeintragung page', function () {
  // before(() => browser.navigateTo(live_url_base));

  // post:
  //   [
  //     'id',             'date',
  //     'date_gmt',       'guid',
  //     'modified',       'modified_gmt',
  //     'slug',           'status',
  //     'type',           'link',
  //     'title',          'excerpt',
  //     'author',         'featured_media',
  //     'parent',         'menu_order',
  //     'comment_status', 'ping_status',
  //     'template',       'meta',
  //     'class_list',     'pgc_meta',
  //     '_links'
  //   ]

  // TODO infer expected types from example response

  let sitemap:
    | {
        pages: (Map<string, any> & { link?: string; id?: number })[];
        posts: (Map<string, any> & { link?: string; id?: number })[];
        categories: Map<string, any>[];
      }
    | undefined = undefined;

  before(async () => {
    try {
      sitemap = JSON.parse(fs.readFileSync(SITEMAP_FILE, { encoding: 'utf8' }));
    } catch (err: any) {
      if (err.code === 'ENOENT') {
        console.info(`going to create file ${SITEMAP_FILE}`);
      } else {
        console.log(err);
        return;
      }
    }

    if (sitemap) return;

    sitemap = await extractSitemap();
  });

  async function extractSitemap() {
    // pagination!
    const pages = await (
      await fetch(`${api_base_url}/pages?per_page=100`)
    ).json();
    const posts = await (
      await fetch(`${api_base_url}/posts?per_page=100`)
    ).json();
    const categories = await (await fetch(`${api_base_url}/categories`)).json();

    sitemap = {
      pages: pages.map((obj: { content: object }) => {
        return { ...obj, content: undefined };
      }),
      posts: posts.map((obj: { content: object }) => {
        return { ...obj, content: undefined };
      }),
      categories,
    };

    // Delete it to re-create.
    fs.exists(SITEMAP_FILE, function (exists: boolean) {
      if (!exists) {
        fs.writeFileSync(SITEMAP_FILE, JSON.stringify(sitemap));
      }
    });
    return sitemap;
  }

  // guid.rendered vs. link, both ought to remain available -> that's a lot of first-level links :-(
  // maybe add a router redirection that 301's everything to one level down if it's not found - yes, let's do that.

  it('compares', async () => {
    // this.timeout(140000);

    const outputArray = [];

    const list =
      sitemap?.pages
        .map((pg) => ({ ...pg, type: 'page' }))
        ?.concat(sitemap?.posts.map((ps) => ({ ...ps, type: 'post' })) ?? []) ??
      [];

    for (const pg of list) {
      // console.log(Object.keys(pg));
      const original_page_url = pg.link;
      if (!original_page_url) {
        console.warn('page has no link?', pg);
        continue;
      }
      const parsed_old = new URL(original_page_url);
      console.log(`processing ${pg.type} ${pg.id}`);
      const new_site_page_url = pg.id
        ? `${new_url_base}/wp/${pg.type}/${pg.id}`
        : rewrite_old_to_new(pg.link);
      // rewrite_old_to_new(pg.guid?.rendered ?? pg.link);
      //  rewrite_old_to_new(pg.link);
      if (!new_site_page_url) {
        console.warn(
          `doesn't seem to be an internal link of the site, skipping: ${original_page_url}`,
        );
        continue;
      }

      console.log(original_page_url, new_site_page_url);

      // const ssFileOld = `screenshots/${runId}-old-${encodeURI(parsed_old.pathname).replaceAll('/', '_')}.png`;
      // const ssFileNew = `screenshots/${runId}-new-${encodeURI(parsed_old.pathname).replaceAll('/', '_')}.png`;

      const ssFileOld = `screenshots/${runId}-old-${pg.type}-${pg.id}.png`;
      const ssFileNew = `screenshots/${runId}-new-${pg.type}-${pg.id}.png`;

      console.log(ssFileOld, ssFileNew);

      browser.navigateTo(original_page_url);
      browser
        .navigateTo(original_page_url)
        .waitForElementPresent('body', 1000, 'Page loaded')
        .saveScreenshot(ssFileOld);

      browser
        .navigateTo(new_site_page_url)
        .waitForElementPresent('body', 1000, 'Page loaded')
        .saveScreenshot(ssFileNew);

      outputArray.push({
        ssFileOld,
        ssFileNew,
        original_page_url,
        new_site_page_url,
      });
    }
    // ...

    const imgBlock = (
      ssFileOld: string,
      ssFileNew: string,
      original_page_url: string,
      new_site_page_url: string,
    ) => {
      return `<tr><td>
      <a href="${original_page_url}">
      <figure><img style="width:100%;" src="${ssFileOld.split('/').at(-1)}"/><figcaption>${ssFileOld}</figcaption></figure>
      </a>
      </td>
      <td>
      <a href="${new_site_page_url}">
      <figure><img style="width:100%;" src="${ssFileNew.split('/').at(-1)}"/><figcaption>${ssFileNew}</figcaption></figure>
      </a>
      </td></tr>`;
    };
    const outputHtml = `<html><body><table style="width:100%;">${outputArray.map((x) => imgBlock(x.ssFileOld, x.ssFileNew, x.original_page_url, x.new_site_page_url)).join('')}</table></body></html>`;

    // fs.writeFileSync('index.html', outputHtml);
    fs.writeFileSync('./screenshots/index.html', outputHtml);
    fs.writeFileSync(`./screenshots/index-${runId}.html`, outputHtml);
  });
});

function rewrite_old_to_new(url?: string): string | undefined {
  if (!url) return;
  const parsed_old = new URL(url);
  const live_url_host = new URL(live_url_base).host;
  if (parsed_old.host !== live_url_host) {
    return;
  }
  return `${new_url_base}/${parsed_old.pathname}`; // or special cases
}
