import type { NightwatchAPI, WindowSizeAndPosition } from 'nightwatch';
import type { Request, Response } from 'express';
import express from 'express';
import type { Express } from 'express';
import type { Server, IncomingMessage, ServerResponse } from 'http';
import { scrollDownToId } from '../common/util';

describe('Teilnahmewunsch/Sammelformular page (mocked backend, mobile edition)', function () {
  // this.tags = ['desktop', 'mobile']; // doesn't run

  let app: Express;
  let server: Server<typeof IncomingMessage, typeof ServerResponse>;

  const url_base = 'http://localhost:4321';
  const start = `${url_base}/sammeln`;
  const url_success = `${url_base}/success`;

  before(async (client, done) => {
    app = express();
    app.post('/submit/send', function (req: Request, res: Response) {
      res.redirect(`${url_success}?success=true`);
    });
    server = app.listen(3001, function () {
      done();
    });
  });

  // the lack of a usable "instanceof" produces "solutions" like this ... how to do this properly?
  function isWindowSizeAndPosition(x: object): x is WindowSizeAndPosition {
    return Object.keys(x).includes('y');
  }

  it('check page title and basic content, heading visibility', () => {
    browser.navigateTo(start);
    browser.assert.titleEquals(
      'Hamburg Werbefrei(Lokal Test) | Sammler*in werden',
    );
    browser.element.find('#wartungstext').assert.not.visible();
    browser.element.find('#heading').assert.visible();
    browser.getElementRect('#heading', function (result) {
      this.assert.equal(typeof result, 'object');
      if (isWindowSizeAndPosition(result.value))
        this.assert.equal(result.value.y < 200, true);
    });
  });

  it('check submit', () => {
    browser.navigateTo(start);
    const button = browser.element.find('form button');
    expect(button).text.toEqual('Absenden');

    button.assert.visible();

    browser.element.find('#name').sendKeys('Max Musterfrau');
    browser.element.find('#email').sendKeys('test@example.com');

    scrollDownToId('absenden');
    browser.element('#absenden').click();

    browser.assert.urlEquals(url_success);

    const successMessageSpan = browser.element.find('#successmessage');
    successMessageSpan.assert.visible();
    expect(successMessageSpan).text.toEqual(
      'Die Eingaben wurden an das Hamburg Werbefrei Team gesendet.',
    );

    browser.assert.titleEquals(
      'Hamburg Werbefrei(Lokal Test) | Sammler*in werden',
    );
  });

  it('run all accessibility rules', () => {
    browser.navigateTo(start);
    browser.axeInject().axeRun('body');
  });

  // should work on every page
  it('check menu button', () => {
    browser.navigateTo(start);
    // browser.element.find('.primary-menu').assert.not.visible();
    // browser.element('#menu-button').click();
    browser.element('.toggle-inner').click();
    // browser.pause(500);
    // browser.element.find('li.menu-item').assert.visible();
  });

  after(() => {
    browser.end();
    server.close();
  });
});
