import type { NightwatchAPI } from 'nightwatch';
import { scrollDownToId } from '../common/util';

describe('Teilnahmewunsch/Sammelformular page', function () {
  const url_base = 'http://localhost:4321';
  const start = `${url_base}/sammeln`;
  const url_success = `${url_base}/success`;
  // const url_success = (value: string) => `${url_base}/success?success=${value}`;

  before(() => browser.navigateTo(start));

  it('Check Sammelformular', () => {
    const button = browser.element.find('form button');
    expect(button).text.toEqual('Absenden');

    button.assert.visible();

    browser.element.find('#name').sendKeys('Max Musterfrau');
    browser.element.find('#email').sendKeys('test@example.com');
    browser.element.find('#radio-newsletter-1-1').click();

    scrollDownToId('absenden');
    browser.element('#absenden').click();

    browser.assert.urlContains(url_success);

    const successMessageSpan = browser.element.find('#successmessage');
    successMessageSpan.assert.visible();
    expect(successMessageSpan).text.toEqual(
      'Die Eingaben wurden an das Hamburg Werbefrei Team gesendet.',
    );
  });

  after(() => browser.end());
});
