import type { NightwatchAPI } from 'nightwatch';
import { scrollDownToId } from '../common/util';

describe('Briefeintragung page', function () {
  const url_base = 'http://localhost:4321';
  const start = `${url_base}/briefeintragung`;
  const url_success = `${url_base}/success_briefwahl`;

  before(() => browser.navigateTo(start));

  it('check form', () => {
    const button = browser.element.find('form button');
    expect(button).text.toEqual('Absenden');

    button.assert.visible();

    browser.element.find('#vorname').sendKeys('Max');
    browser.element.find('#nachname').sendKeys('Musterfrau');
    browser.element.find('#strassenr').sendKeys('Beispielstr. 1');
    browser.element.find('#plz').sendKeys('20095');

    scrollDownToId('absenden');
    browser.element('#absenden').click();

    browser.assert.urlEquals(start);

    scrollDownToId('beantragung');
    browser.element.find('#beantragung').click();
    browser.element.find('#datenschutz').click();

    scrollDownToId('absenden');
    browser.element('#absenden').click();

    browser.assert.urlEquals(url_success);

    const successMessageSpan = browser.element.find('#successmessage');
    successMessageSpan.assert.visible();
    expect(successMessageSpan).text.toEqual(
      'Die Eingaben wurden an das Hamburg Werbefrei Team gesendet.',
    );
  });

  after(() => browser.end());
});
