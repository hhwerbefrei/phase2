import { defineConfig, envField } from 'astro/config';

import node from '@astrojs/node';
import scope from 'astro-scope';

export default defineConfig({
  experimental: {},

  optimizeDeps: {
    exclude: ['@rollup/browser'],
  },

  env: {
    schema: {
      BASE_URL: envField.string({
        context: 'client',
        access: 'public',
        optional: true,
        default: 'https://staging.hwf.wfrei.de/',
      }),
      API_BASE_URL: envField.string({
        context: 'client',
        access: 'public',
        optional: true,
        default: 'https://api.staging.hwf.wfrei.de/',
      }),
      TITLE_REMARK: envField.string({
        context: 'client',
        access: 'public',
        optional: true,
      }),
      WARTUNGSTEXT: envField.string({
        context: 'client',
        access: 'public',
        optional: true,
      }),
      WP_URL: envField.string({
        context: 'client',
        access: 'public',
        optional: true,
        default: 'https://www.hamburg-werbefrei.de/graphql',
      }),
    },
  },

  security: {
    checkOrigin: true,
  },

  output: 'server',

  adapter: node({
    mode: 'standalone',
  }),

  integrations: [scope()],

  //  define special mappings
  redirects: {
    '/splash': '/',
    // TODO deal with *all* categories from wp;
    '/gruende/vorher-nachher': '/vorher-nachher',
    // substituted pages:
    '/wp/post/4023': '/vorher-nachher',
    '/wp/page/4284': '/',
    '/wp/page/3837': '/newsletter',
    // old top-level permalinks:
    '/privacy-policy-2': '/wp/page/3501',
    '/impressum-2': '/wp/page/3561',

    '/spenden': '/wp/page/4763',
    '/flyer-sticker-poster': '/uncategorized/flyer-sticker-poster',
    '/presse': '/uncategorized/presse',
    '/abgeordnetenwatch': '/wp/page/5922',
    '/signalgruppen': '/wp/page/5802',
    '/bilder-vom-sammelstart': '/wp/page/4552',
    '/jetzt-unterschreiben': '/wp/page/4541',
    '/gesetzentwurf': '/wp/page/4511',
    '/ueber-uns': '/wp/page/3477',

    '/pressemitteilung-der-volksinitiative-hamburg-werbefrei-ein-sieg-fuer-die-demokratie-und-fuer-die-gesamte-stadt':
      '/wp/post/5601',
    '/warten-auf-urteil-am-6-9-24': '/wp/post/5572',
    '/karte': '/wp/post/5539',
    '/kunst-statt-werbung': '/wp/post/5527',
    '/endlich-ein-termin-fuer-die-muendliche-verhandlung-beim-landesverfassungericht':
      '/wp/post/5519',
    '/attac-hamburg-unterstuetzt-hamburg-werbefrei': '/wp/post/5488',
    '/senat-verklagt-hamburg-werbefrei-vor-dem-verfassungsgericht-scharfe-kritik-an-spd-und-gruenen':
      '/wp/post/5459',
    '/teilnahme-an-fallschirmaktion-von-parents-for-future': '/wp/post/5412',
    '/senat-bestaetigt-erfolg-von-hamburg-werbefrei': '/wp/post/5343',
    '/volksinitiative-hamburg-werbefrei-erfolgreich-forderung-nach-sofortigem-werbestopp':
      '/wp/post/5291',
    '/sammeldemo-am-15-10': '/wp/post/5230',
    '/werbeindustrie-raeumt-hohen-energieverbrauch-ein-politik-muss-sofort-handeln':
      '/wp/post/5086',
    '/waermebilder-beweisen-werbemonitore-verschwenden-energie-und-heizen-staedte-auf':
      '/wp/post/5080',
    '/bergfest-5000-unterschrift-und-verhuellung-eines-werbemonitors':
      '/wp/post/5029',
    '/diese-organisationen-unterstuetzen-hamburg-werbefrei': '/wp/post/5008',

    '/halbzeit-wir-koennen-es-schaffen': '/wp/post/4968',
    '/9-6-podiumsdiskussion-an-der-tuhh': '/wp/post/4726',
    '/11-6-um-15-uhr-offenes-treffen-fuer-interessierte-centro-sociale':
      '/wp/post/4679',
    '/25-5-19-uhr-werbefreie-kneipe-jupi-bar': '/wp/post/4674',
    '/ausfuehrliches-radiofeature-bei-deutschlandfunk': '/wp/post/4532',
    '/montag-25-4-sammelstart-vor-dem-rathaus': '/wp/post/4530',
    '/3-treffen-fuer-interessierte-am-30-04-2022-samstag-1500-1700':
      '/wp/post/4522',
    '/beitrag-bei-hamburg1': '/wp/post/4508',
    '/4249': '/wp/post/4249',
    '/ein-vorgeschmack': '/wp/post/4232',
    '/2-treffen-fuer-interessierte-am-19-02-2022-samstag-1500-1700-nur-online':
      '/wp/post/4220',
    '/hamburg-werbefrei-stellt-sich-vor': '/wp/post/4205',
    '/1-treffen-fuer-interessierte': '/wp/post/4200',
    '/werbebelastung-in-hamburg': '/wp/post/4040',
    '/senat-beantwortet-fragen-zur-vertragsverlaengerung-nicht':
      '/wp/post/3998',
    '/pressemitteilung-zu-heimlicher-senatsentscheidung': '/wp/post/3970',
    '/pressespiegel-12-6-2021': '/wp/post/3941',
    '/diskussionsbeitrag': '/wp/post/3928',
    // TODO generell den <- / -> wiederherstellen, evtl.
    '/pressespiegel': '/wp/post/3902', // links zu spaeteren?

    // neue permalinks
    '/impressum': '/wp/page/3561',
    '/privacy-policy': '/wp/page/3501',
  },
});
