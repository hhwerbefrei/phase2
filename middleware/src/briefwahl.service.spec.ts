import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { BriefwahlService } from './briefwahl.service';
import { FileService } from './fs/fileservice';
import { MailerService } from './mail/mailerservice';
import { BriefwahlFormDto } from './messages/BriefwahlFormDto';
import { decryptData } from './util/crypto';
import { ConfigurationError } from '@/errors';

const public_key =
  '-----BEGIN PUBLIC KEY-----\n\
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApGTZhkCz0RHESTaXmUfz\n\
iUh+WwjhYluPxdgCUpGMiS1e8Ew4u5oPCDE0kqNNFLspWTWwKErF933AnVEpCdmK\n\
SEMRCw4BasQ4XwmwrUikmtrxheTKNXXGdwO0OG3AIzhvVVlnyaiKx3VsJzwGyVKm\n\
kV+IQll+by0oVZTLgGC1CeHsYvpZsZk/Zg8MGXyNMz+Rb4Fnm6RcX4nV2kctTQI8\n\
9BC6NL/M0ix8spjvGl5Qib2ipuuB8N7qyfdKki56aPoMsLw1BdqpmHEf0UF6Qyzh\n\
9ZZraUtdDNXOr9CnzLFwoY6wNCAg87PtnQ2Vgg+bEXIInKSh/EqwgkDGzrBfopUQ\n\
VQIDAQAB\n\
-----END PUBLIC KEY-----';

const private_key =
  '-----BEGIN PRIVATE KEY-----\n\
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCkZNmGQLPREcRJ\n\
NpeZR/OJSH5bCOFiW4/F2AJSkYyJLV7wTDi7mg8IMTSSo00UuylZNbAoSsX3fcCd\n\
USkJ2YpIQxELDgFqxDhfCbCtSKSa2vGF5Mo1dcZ3A7Q4bcAjOG9VWWfJqIrHdWwn\n\
PAbJUqaRX4hCWX5vLShVlMuAYLUJ4exi+lmxmT9mDwwZfI0zP5FvgWebpFxfidXa\n\
Ry1NAjz0ELo0v8zSLHyymO8aXlCJvaKm64Hw3urJ90qSLnpo+gywvDUF2qmYcR/R\n\
QXpDLOH1lmtpS10M1c6v0KfMsXChjrA0ICDzs+2dDZWCD5sRcgicpKH8SrCCQMbO\n\
sF+ilRBVAgMBAAECggEALkLtDcEmeQaZaIry1WEwkOj0GdUha8bdaizz1l18IVxB\n\
s7iXXH+pjEIgi8VlmxhiNecMWAJWvGNIcVzWAh4UMmqhIK3Dy1JFlMUK0XC4VZWY\n\
UvgDyVCH1ZUWwaxs3dxzRB7hPJfv2dpa4Z2cQUo/cnhiRV2e3VVlXNP/AnT0bT+Q\n\
qrzPemE/gp/GPzxTvU0uuy0FG8RdOiFZ4BKoC1GE9SjZSYHNFhO0y3J0LyqtxgF8\n\
c44jl2OIY3Z4R07pqltLiMefYIJCyrn/vKJRz1rbyg2kuMVyVgSxje/vqnyTKR6v\n\
Ehdafy8HhBawQlj+A0rcpeu/qWwUaWI5uxnhhpVfzwKBgQDn/1WaSyxq8343curK\n\
qesIk9080imT0uoqiluPtouLP0RarSkNcKJdYR9NHoD39qlHrUPLWoufGs0iTIRf\n\
PuqrgqRuG40mX2f1IDKCK7+2DVYrylzUqfNk4VFNRwRik7z+c8niBJZefQRdQ7ox\n\
9po1JauRtWpeYphm550MJzelywKBgQC1Zvms6jjOM+5JQfkOOkdzieui6LgaS2OS\n\
R2WifPMT6yw+Lr4Opmm0gIHj3kW0PCrGFI4ir1nfEpNGToSUSTdruoK93rXS8QKi\n\
4pzgClBDKoF+BFuWUsEa2Yv/bB/sx1AVwMZzl03T0gr6TllK0Aa81bZH8bTFgoQj\n\
AJLEAGpeXwKBgQCJuaEd+eaij26siEIukfT/oJhEVRdttFZjbsOwa1QoFCUGF6o5\n\
WlZpTREB/Uve6MXR0WBfxp8Aky6YYAhtarJxoxruati+oQDcGoMhl8s1znELihTW\n\
AxaFGuXjFHfb9YzLGf4NeV+zNFB09ZIBuz4MsxJJbAN7iNikrncjEd0NRQKBgHF8\n\
+A0XB+zxjk1kqdUd3t/I1JO59pv0uFmms04hz82RXr+UbLErqYwUra6Ku58T2UrN\n\
R1Mp0xb9PwhITSecWsDaEbuJ3sUWpSiUj8KWkTpEGHlnbokwQsFyguSMl7iBxXem\n\
gEGG0lkH2nhYlgJ4/Q4cwgpmjErY6aQgUrGu9N0PAoGAZXm0Ll6aQuoB4P/5OKZm\n\
QjpH9iKzjZeKezvXfXCe9iU6Ud2RlCh01yGugJKLDrbUZHPcSqKTvsURbjjwHSDy\n\
WUFhXd0igNOf6mUGYuY1+DIE0TC2P3YTsw8enj1f4i+hC34uutFlEwIk4dp+aXlO\n\
Pb3MGgfb7Tl5R/LXjvyZ7ks=\n\
-----END PRIVATE KEY-----\
';

function decryptEntry(data: { sk: string; payload: string; iv: string }) {
  return decryptData(private_key, data);
}

const eintraege: BriefwahlFormDto[] = [
  {
    vorname: 'Max',
    nachname: 'Musterfrau',
    beantragung: 'true',
    plz: '22527',
    strassenr: 'Vogt-Kölln-Str. 1024',
  },
  {
    vorname: 'Moritz',
    nachname: 'Musterfrau',
    beantragung: 'true',
    plz: '22527',
    strassenr: 'Vogt-Kölln-Str. 1024',
  },
  {
    vorname: 'Klaus',
    nachname: 'Störtebeker',
    beantragung: 'true',
    plz: '20359',
    strassenr: 'Bei den St.Pauli-Landungsbrücken 1',
  },
  {
    vorname: 'Maria',
    nachname: 'Störtebeker',
    beantragung: 'true',
    plz: '20359',
    strassenr: 'Bei den St.Pauli-Landungsbrücken 1',
    email: 'test@example.com',
  },
];

const mailFrom = 'sender@sender.example.com';
const mailTo = 'sammelstelle@sammlung.example.com';

describe('BriefwahlService', () => {
  afterEach(jest.clearAllMocks);

  const mockMailInfo = {
    envelope: { from: '', to: [] },
    messageId: 'mymessageid',
    accepted: ['foo@foo.foo'],
    rejected: [],
    pending: [],
    response: 'responsefrommail',
  };

  async function setupService(extraEnv = {}) {
    const logEntryMockFn = jest.fn();
    const sendMockFn = jest.fn().mockReturnValue(mockMailInfo);

    const getEnv = () => ({
      MAIL_FROM: mailFrom,
      MAIL_TO_BRIEFWAHL: mailTo,
      ENCRYPT_BRIEFWAHL_MAIL: 'false',
      BRIEFWAHL_LOG_PATH: '/foo/bar/test_briefwahl_log.txt',
      TEILNAHMEWUNSCH_SUBJECT: 'Teilnahmewunsch',
      VERBOSE: 'false',
      ...extraEnv,
    });

    const app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          load: [getEnv],
          envFilePath: [],
        }),
      ],
      providers: [
        ConfigService,
        BriefwahlService,
        {
          provide: FileService,
          useValue: {
            logEntry: logEntryMockFn,
          },
        },
        {
          provide: MailerService,
          useValue: {
            sendMail: sendMockFn,
          },
        },
      ],
    }).compile();

    return {
      briefwahlService: app.get<BriefwahlService>(BriefwahlService),
      logEntryMockFn,
      sendMockFn,
    };
  }

  it('requires encryption key to be set', async () => {
    const mustFail = () => setupService({});
    expect(mustFail).rejects.toThrow();
  });

  it.each([
    [
      {
        ENCRYPTION_KEY: public_key,
        BRIEFWAHL_SUBJECT: 'Briefeintragung (TEST)',
      },
      '[briefeintragung] Briefeintragung (TEST)',
      false,
    ],
    [
      { ENCRYPTION_KEY: public_key },
      '[briefeintragung] Briefeintragung',
      false,
    ],
    [
      { ENCRYPTION_KEY: public_key, ENCRYPT_BRIEFWAHL_MAIL: 'true' },
      '[briefeintragung] Briefeintragung',
      true,
    ],
  ])('works (happy path)', async (extraEnv, mailSubject, mailEncrypted) => {
    const { briefwahlService, logEntryMockFn, sendMockFn } = await setupService(
      extraEnv,
    );
    for (const eintrag of eintraege) {
      const timestamp = new Date().toISOString();
      await briefwahlService.secureLogRequest(eintrag, timestamp, undefined);
      expect(logEntryMockFn).toHaveBeenCalled();

      const arg = logEntryMockFn.mock.calls.at(-1)[0];
      const entry = JSON.parse(Buffer.from(arg, 'base64').toString());

      expect(entry.timestamp).toEqual(timestamp);

      const decrypted = JSON.parse(
        decryptEntry(entry.encryptedData).toString(),
      );
      expect(decrypted).toEqual(expect.objectContaining(eintrag));

      await briefwahlService.sendMail(eintrag);

      expect(sendMockFn).toHaveBeenCalled();

      const mailArg = sendMockFn.mock.calls.at(-1)[0];

      expect(mailArg.subject).toEqual(mailSubject);

      expect(JSON.parse(mailArg.text)).toEqual(
        mailEncrypted
          ? expect.objectContaining({
              iv: expect.stringMatching(new RegExp('.')),
              sk: expect.stringMatching(new RegExp('.')),
              payload: expect.stringMatching(new RegExp('.')),
            })
          : expect.objectContaining(eintrag),
      );

      if (mailEncrypted) {
        expect(mailArg.html).toBeUndefined();
      } else {
        expect(mailArg.html).toContain(eintrag.nachname);
        if (eintrag.email) {
          expect(mailArg.html).toContain(eintrag.email);
        }
      }
      expect(mailArg.to).toEqual(mailTo);
      expect(mailArg.from).toEqual(mailFrom);
    }
  });
});
