import { Test } from '@nestjs/testing';
import { mockRequest } from 'mock-req-res';
import { BriefwahlController } from './briefwahl.controller';
import { BriefwahlFormDto } from './messages/BriefwahlFormDto';
import { BriefwahlService } from './briefwahl.service';
import { MailerServiceError } from './mail/mailerservice';
import { HttpStatus } from '@nestjs/common';
import { mockMailInfo, mockMailInfo_bad } from '../test/common';

describe('BriefwahlController', () => {
  afterEach(jest.clearAllMocks);

  async function setupController(
    mailInfo:
      | (typeof mockMailInfo | typeof mockMailInfo_bad)
      | MailerServiceError = mockMailInfo,
    log_ok = true,
  ) {
    const sendMockFn = jest.fn().mockReturnValue(mailInfo);
    const secureLogMockFn = jest.fn().mockResolvedValue(log_ok);

    const app = await Test.createTestingModule({
      controllers: [BriefwahlController],
      providers: [
        {
          provide: BriefwahlService,
          useValue: {
            sendMail: sendMockFn,
            secureLogRequest: secureLogMockFn,
          },
        },
      ],
    }).compile();

    return {
      app: app.get<BriefwahlController>(BriefwahlController),
      sendMock: sendMockFn,
      secureLogMock: secureLogMockFn,
    };
  }

  describe('handling of requests', () => {
    const defaultFormInput: BriefwahlFormDto = {
      vorname: 'Max',
      nachname: 'Musterfrau',
      beantragung: 'true',
      plz: '22527',
      strassenr: 'Vogt-Kölln-Str. 1024',
    };

    const request = mockRequest({
      query: {
        redirect: 'foo',
      },
    });

    it('error case should return 500 if no redirect url can be found', async () => {
      const { app } = await setupController(
        new MailerServiceError(new Error('something went wrong')),
      );

      const request = mockRequest({});

      expect(await app.submitBriefwahlForm(defaultFormInput, request)).toEqual({
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      });
    });

    it('error case should redirect with success=false', async () => {
      const { app, sendMock } = await setupController(
        new MailerServiceError(new Error('something went wrong')),
      );

      expect(await app.submitBriefwahlForm(defaultFormInput, request)).toEqual({
        statusCode: 302,
        url: 'foo?success=false',
      });
      expect(sendMock).toHaveBeenCalledWith(
        expect.objectContaining({
          ...defaultFormInput,
          timestamp: expect.anything(),
        }),
      );
    });

    it('should redirect with success=false param if mail rejected', async () => {
      const { app, sendMock } = await setupController(mockMailInfo_bad);

      expect(await app.submitBriefwahlForm(defaultFormInput, request)).toEqual({
        statusCode: 302,
        url: 'foo?success=false',
      });
      expect(sendMock).toHaveBeenCalledWith(
        expect.objectContaining({
          ...defaultFormInput,
          timestamp: expect.anything(),
        }),
      );
    });

    it('should redirect with success=false param if logging fails for some reason', async () => {
      const { app, sendMock } = await setupController(mockMailInfo, false);

      expect(await app.submitBriefwahlForm(defaultFormInput, request)).toEqual({
        statusCode: 302,
        url: 'foo?success=false',
      });
      expect(sendMock).toHaveBeenCalledWith(
        expect.objectContaining({
          ...defaultFormInput,
          timestamp: expect.anything(),
        }),
      );
    });

    it("can't redirect when no redirect param is given", async () => {
      const { app, sendMock } = await setupController(mockMailInfo);

      expect(
        await app.submitBriefwahlForm(defaultFormInput, mockRequest({})),
      ).toEqual({
        statusCode: 202,
      });
      expect(sendMock).toHaveBeenCalledWith(
        expect.objectContaining({
          ...defaultFormInput,
          timestamp: expect.anything(),
        }),
      );
    });

    it('should accept message if send succeeds', async () => {
      const { app, sendMock } = await setupController(mockMailInfo);

      expect(await app.submitBriefwahlForm(defaultFormInput, request)).toEqual({
        statusCode: 302,
        url: 'foo?success=true',
      });
      expect(sendMock).toHaveBeenCalledWith(
        expect.objectContaining({
          ...defaultFormInput,
          timestamp: expect.anything(),
        }),
      );
    });

    it.each([
      [{}, 'foo', 'unknown', 'foo?success=true'],
      [
        { 'x-forwarded-for': '2001:db8::1:0' },
        'foo',
        '2001:db8::1:0',
        'foo?success=true',
      ],
      [
        { 'x-forwarded-for': '2001:db8::1:0' },
        'foo?bla=blub',
        '2001:db8::1:0',
        'foo?bla=blub&success=true',
      ],
    ])(
      'should accept message and send briefeintragung request',
      async (
        extraHeaders,
        redirect,
        loggedIp: string,
        expectedRedirectUrl: string,
      ) => {
        const { app, sendMock } = await setupController(mockMailInfo);

        const formInput = { ...defaultFormInput };

        const request = mockRequest({
          query: {
            redirect,
          },
          headers: extraHeaders,
        });

        expect(await app.submitBriefwahlForm(formInput, request)).toEqual({
          statusCode: 302,
          url: expectedRedirectUrl,
        });
        expect(sendMock).toHaveBeenCalledWith(
          expect.objectContaining({
            ...formInput,
            timestamp: expect.anything(),
            remote_ip: loggedIp,
          }),
        );
      },
    );

    it('should process e-mail field as well', async () => {
      const { app, sendMock, secureLogMock } = await setupController(
        mockMailInfo,
      );

      const formInput = {
        ...defaultFormInput,
        email: 'musterfrau@example.com',
      };

      const request = mockRequest({
        query: {
          redirect: 'foo',
        },
      });

      expect(await app.submitBriefwahlForm(formInput, request)).toEqual({
        statusCode: 302,
        url: 'foo?success=true',
      });

      expect(sendMock).toHaveBeenCalledWith(
        expect.objectContaining({
          ...formInput,
          timestamp: expect.anything(),
          remote_ip: 'unknown',
        }),
      );

      expect(secureLogMock).toHaveBeenCalledWith(
        expect.objectContaining(formInput),
        expect.stringMatching(new RegExp('20..-..-..T')),
        'unknown',
      );
    });
  });
});
