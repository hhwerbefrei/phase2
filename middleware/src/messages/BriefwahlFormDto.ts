import {
  IsBooleanString,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class BriefwahlFormDto {
  @IsString()
  @IsNotEmpty()
  vorname?: string;

  @IsString()
  @IsNotEmpty()
  nachname?: string;

  @IsString()
  @IsNotEmpty()
  strassenr?: string;

  @IsString()
  @IsNotEmpty()
  plz?: string;

  @IsString()
  @IsOptional()
  email?: string;

  @IsBooleanString()
  @IsNotEmpty()
  beantragung?: string;
}
