import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class FormDto {
  @IsString()
  @IsNotEmpty()
  name?: string;

  @IsEmail()
  @IsNotEmpty()
  email?: string;

  @IsString()
  @IsOptional()
  newsletter?: string;

  @IsString()
  @IsOptional()
  signal?: string;

  @IsString()
  @IsOptional()
  phone?: string;

  @IsString()
  @IsOptional()
  nachricht?: string;

  @IsString()
  @IsOptional()
  stadtteil?: string;

  @IsString()
  @IsOptional()
  anwerben?: string;

  @IsString()
  @IsOptional()
  erfahrung?: string;

  @IsString()
  @IsOptional()
  zeitumfang?: string;

  @IsString()
  @IsOptional()
  zum_naechsten_treffen?: string;
}
