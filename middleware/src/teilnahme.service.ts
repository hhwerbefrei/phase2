import { Injectable } from '@nestjs/common';
import { FormDto } from './messages/FormDto';
import { ConfigService } from '@nestjs/config';
import { MailerService, MailerServiceError } from './mail/mailerservice';
import { escape } from 'html-escaper';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

// in the future, if the problem persists, read this list from a config file
function emailKnownUsedBySpammers(email: string) {
  return (
    /lasslc\.org$/.test(email) || /minnesotaorthodontics\.com$/.test(email)
  );
}

@Injectable()
export class TeilnahmeService {
  constructor(
    private readonly configService: ConfigService,
    private readonly mailerService: MailerService,
  ) {}

  public async processNewsletterRequest(formDto: FormDto): Promise<boolean> {
    const emailToSendNewsletterTo = formDto.email ?? '';
    // simply return true, don't tell them it hasn't worked
    if (emailKnownUsedBySpammers(emailToSendNewsletterTo)) {
      console.log(`spammy email dropped, ${emailToSendNewsletterTo}`);
      return true;
    }
    if (
      formDto.newsletter?.toLowerCase().match(new RegExp('ja|true')) &&
      /@/.test(emailToSendNewsletterTo)
    ) {
      return await this.mailerService.submitNewsletterRequest(
        emailToSendNewsletterTo,
      );
    }
    return false;
  }

  public async sendMail(
    formDto: FormDto,
  ): Promise<SMTPTransport.SentMessageInfo | MailerServiceError> {
    const subject = `[sammelformular] ${
      this.configService.get('TEILNAHMEWUNSCH_SUBJECT') ?? 'Teilnahmewunsch'
    }`;

    const html = Object.entries(formDto)
      .map(([key, value]) => `<p>${key} = ${escape(value)}</p>`)
      .join('');

    const info = await this.mailerService.sendMail({
      to: this.configService.get('MAIL_TO'),
      from: this.configService.get('MAIL_FROM'),
      subject,
      text: JSON.stringify(formDto),
      html,
    });

    return info;
  }
}
