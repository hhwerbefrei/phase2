import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { TeilnahmeService } from './teilnahme.service';
import { MailerService } from './mail/mailerservice';
import { FormDto } from './messages/FormDto';

const mockMailInfo = {
  envelope: { from: '', to: [] },
  messageId: 'mymessageid',
  accepted: ['foo@foo.foo'],
  rejected: [],
  pending: [],
  response: 'responsefrommail',
};

const mailFrom = 'sender@sender.example.com';
const mailTo = 'sammelstelle@sammlung.example.com';

describe('AppService', () => {
  const getEnv = () => ({
    VERBOSE: 'true',
    MAIL_FROM: mailFrom,
    MAIL_TO: mailTo,
  });

  afterEach(jest.clearAllMocks);
  async function setupService() {
    const sendMockFn = jest.fn().mockReturnValue(mockMailInfo);
    const submitMockFn = jest.fn().mockResolvedValue(true);

    const app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          load: [getEnv],
          envFilePath: [],
        }),
      ],
      providers: [
        TeilnahmeService,
        ConfigService,
        {
          provide: MailerService,
          useValue: {
            sendMail: sendMockFn,
            submitNewsletterRequest: submitMockFn,
          },
        },
      ],
    }).compile();

    return {
      appService: app.get<TeilnahmeService>(TeilnahmeService),
      sendMockFn,
      submitMockFn,
    };
  }

  describe('sendMail', () => {
    it('calls send function and returns mail info', async () => {
      const { appService, sendMockFn } = await setupService();
      const input: FormDto = {
        stadtteil: 'Öjendorf',
      };
      const result = await appService.sendMail(input);
      expect(sendMockFn).toHaveBeenCalledWith(
        expect.objectContaining({
          subject: '[sammelformular] Teilnahmewunsch',
        }),
      );

      const mailArg = sendMockFn.mock.calls.at(-1)[0];
      expect(JSON.parse(mailArg.text)).toEqual(expect.objectContaining(input));
      expect(mailArg.to).toEqual(mailTo);
      expect(mailArg.from).toEqual(mailFrom);

      expect(result).toEqual(mockMailInfo);
    });
  });
  describe('processNewsletterRequest', () => {
    it('calls submitNewsletterRequest when called with newsletter box checked and address given', async () => {
      const { appService, submitMockFn } = await setupService();
      const input: FormDto = { newsletter: 'ja', email: 'foo@example.com' };
      const result = await appService.processNewsletterRequest(input);
      expect(submitMockFn).toHaveBeenCalled();
      expect(result).toEqual(true);
    });

    it.each([
      { newsletter: 'nein', email: 'foo@example.com' },
      { email: 'foo@example.com' },
    ])(
      'does not call submitNewsletterRequest otherwise',
      async (input: FormDto) => {
        const { appService, submitMockFn } = await setupService();
        const result = await appService.processNewsletterRequest(input);
        expect(submitMockFn).not.toHaveBeenCalled();
        expect(result).toEqual(false);
      },
    );
  });
});
