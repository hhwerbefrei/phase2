import {
  Body,
  Controller,
  Header,
  HttpStatus,
  Post,
  Redirect,
  Req,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { TeilnahmeService } from './teilnahme.service';
import { isMailerServiceError } from './mail/mailerservice';
import { FormDto } from './messages/FormDto';
import { Request } from 'express';
import { SubscribeNewsletterDto } from './messages/SubscribeNewsletterDto';
import { to_single_string } from './util/util';
import { ConfigService } from '@nestjs/config';

@Controller('submit')
export class AppController {
  constructor(
    private readonly appService: TeilnahmeService,
    private readonly configService: ConfigService,
  ) {}

  private get verbose() {
    return ['1', 'true', 'yes'].includes(
      this.configService.get('VERBOSE', 'false'),
    );
  }

  @Post('subscribeNewsletter')
  @Redirect()
  @UsePipes(new ValidationPipe())
  @Header('Cache-Control', 'none')
  async subscribeNewsletter(
    @Body() message: SubscribeNewsletterDto,
    @Req() request?: Request,
  ) {
    if (this.verbose) {
      const remote_ip =
        to_single_string(request?.headers['x-forwarded-for']) || 'unknown';

      if (new Date().getHours() < 6) {
        console.debug(`newsletter request from ${remote_ip}`);
      }
    }

    const success = await this.appService.processNewsletterRequest({
      ...message,
      newsletter: 'true',
    });

    const redirect = request?.query['redirect'] || request?.headers.referer;
    const redirectUri =
      redirect !== undefined && typeof redirect === 'string'
        ? decodeURI(redirect)
        : undefined;

    const successParam = `${
      /[?]/.test(redirectUri ?? '') ? '&' : '?'
    }success=${success}`;

    return redirectUri
      ? {
          statusCode: HttpStatus.FOUND,
          url: `${redirectUri}${successParam}`,
        }
      : { statusCode: HttpStatus.ACCEPTED };
  }

  @Post('send')
  @Redirect()
  @UsePipes(new ValidationPipe())
  @Header('Cache-Control', 'none')
  async submitForm(@Body() message: FormDto, @Req() request?: Request) {
    const info = await this.appService.sendMail(message);

    await this.appService.processNewsletterRequest(message);

    const redirect = request?.query['redirect'] || request?.headers.referer;
    if (redirect === undefined || typeof redirect !== 'string') {
      if (isMailerServiceError(info)) {
        return {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        };
      }
      return { statusCode: HttpStatus.ACCEPTED };
    }
    const redirectUri = decodeURI(redirect);
    const success = isMailerServiceError(info)
      ? 'false'
      : info.rejected?.length == 0
      ? 'true'
      : 'false';
    const successParam = `${
      /[?]/.test(redirectUri) ? '&' : '?'
    }success=${success}`;

    return {
      statusCode: HttpStatus.FOUND,
      url: `${redirectUri}${successParam}`,
    };
  }
}
