import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const defaultListenPort = 3001;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);

  await app.listen(
    parseInt(config.get('LISTEN_PORT', defaultListenPort.toString())),
  );
}
bootstrap();
