import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Mail from 'nodemailer/lib/mailer';
import * as nodemailer from 'nodemailer';
type NodemailerType = typeof nodemailer;
import SMTPTransport from 'nodemailer/lib/smtp-transport';

import axios from 'axios';
import { ConfigurationError } from '../errors';

export class MailerServiceError {
  isMailerServiceError = true;
  what: Error;
  constructor(what: Error) {
    this.what = what;
  }
}

export function isMailerServiceError(arg: any): arg is MailerServiceError {
  return arg.isMailerServiceError !== undefined;
}

@Injectable()
export class MailerService {
  private newsletterAbonnierenUrl: string;

  constructor(
    private readonly configService: ConfigService,
    @Inject('nodemailer') private nodemailer: NodemailerType,
  ) {
    const newsletterAbonnierenUrl = this.configService.get(
      'NEWSLETTER_ABONNIEREN_URL',
    );
    if (!newsletterAbonnierenUrl) {
      throw new ConfigurationError(
        'NEWSLETTER_ABONNIEREN_URL required but not set',
      );
    } else {
      this.newsletterAbonnierenUrl = newsletterAbonnierenUrl;
    }
  }

  private get verbose(): boolean {
    return ['1', 'true', 'yes'].includes(
      this.configService.get('VERBOSE', 'false'),
    );
  }

  public async submitNewsletterRequest(email: string): Promise<boolean> {
    this.verbose && console.log('submitting newsletter request');

    try {
      const formData = { email, liste: 'hamburg-werbefrei' };

      await axios.post(this.newsletterAbonnierenUrl, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        timeout: 25000,
      });
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error('axios error: ', error.message);
      } else {
        console.error('unexpected error: ', error);
      }
      return false;
    }

    return true;
  }

  public async sendMail(
    mailOptions: Mail.Options,
  ): Promise<SMTPTransport.SentMessageInfo | MailerServiceError> {
    this.verbose && console.log(this.configService.get('MAIL_USER'));

    let mailPort = 465;
    try {
      mailPort = parseInt(this.configService.get('MAIL_PORT') ?? '465');
    } catch {
      this.verbose && console.log(`mail port: defaulting to ${mailPort}`);
    }

    const transport = {
      host: this.configService.get('MAIL_HOST'),
      port: mailPort,
      secure: mailPort === 587 ? false : true,
      auth: {
        user: this.configService.get('MAIL_USER'),
        pass: this.configService.get('MAIL_PASS'),
      },
    };

    const transporter = this.nodemailer.createTransport(transport);

    transporter.verify(function (error) {
      if (error) {
        console.log(error);
        return new MailerServiceError(error);
      } else {
        console.log('MailerService: Server is ready to take our messages');
      }
    });

    const info = await transporter.sendMail(mailOptions);

    this.verbose &&
      console.log(
        'Message sent: %s',
        JSON.stringify({ response: info.response, messageId: info.messageId }),
      );

    return info;
  }
}
