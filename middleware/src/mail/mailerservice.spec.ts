import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import axios from 'axios';
import { MailerService } from './mailerservice';
import Mail from 'nodemailer/lib/mailer';

jest.mock('axios');

const newsletterAbonnierenUrl =
  'https://api.example.com/mailing-list/subscribe/';

const sendMailMock = jest.fn((mail: Mail) => {
  return Promise.resolve(mail);
});

const verifyMock = jest.fn(() => {
  return true;
});

const transporterMock = {
  verify: verifyMock,
  sendMail: sendMailMock,
};

const nodemailerMock = {
  createTransport: jest.fn().mockReturnValue(transporterMock),
};

jest.mock('nodemailer', () => nodemailerMock);

async function setupService() {
  const module = await Test.createTestingModule({
    imports: [ConfigModule.forRoot()],
    providers: [
      ConfigService,
      MailerService,
      { provide: 'nodemailer', useFactory: () => nodemailerMock },
    ],
  }).compile();

  return {
    mailerService: module.get<MailerService>(MailerService),
  };
}

describe('mailerservice', () => {
  beforeEach(() => {
    process.env = {
      VERBOSE: 'false',
      NEWSLETTER_ABONNIEREN_URL: newsletterAbonnierenUrl,
      BRIEFWAHL_LOG_PATH: '/foo/bar/test_briefwahl_log.txt',
    };
  });

  describe('submitNewsletterRequest', () => {
    it('works', async () => {
      const { mailerService } = await setupService();
      await mailerService.submitNewsletterRequest('foo@example.com');
      expect(axios.post).toHaveBeenCalledWith(
        newsletterAbonnierenUrl,
        expect.objectContaining({ email: 'foo@example.com' }),
        expect.objectContaining({}),
      );
    });
  });

  describe('sendMail', () => {
    it('works', async () => {
      const { mailerService } = await setupService();
      await mailerService.sendMail({ to: 'test@example.com' });
      expect(sendMailMock).toHaveBeenCalled();
      expect(verifyMock).toHaveBeenCalled();
    });
  });
});
