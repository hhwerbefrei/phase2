import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MailerService, MailerServiceError } from './mail/mailerservice';
import { escape } from 'html-escaper';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import { BriefwahlFormDto } from './messages/BriefwahlFormDto';
import { createPublicKey, KeyObject } from 'crypto';
import { FileService } from './fs/fileservice';
import { encryptData } from './util/crypto';
import { ConfigurationError } from './errors';

export enum BriefwahlErrorType {
  EncryptionError = 'EncryptionError',
}

export class BriefwahlServiceError {
  isBriefwahlServiceError = true;
  type: BriefwahlErrorType;
  what: string;
  constructor(type: BriefwahlErrorType, what: string) {
    this.type = type;
    this.what = what;
  }
}

export function isBriefwahlServiceError(
  arg: any,
): arg is BriefwahlServiceError {
  return arg.isBriefwahlServiceError !== undefined;
}

@Injectable()
export class BriefwahlService {
  encryptionKey: KeyObject;

  constructor(
    private readonly configService: ConfigService,
    private readonly mailerService: MailerService,
    private readonly fileService: FileService,
  ) {
    const key = this.configService.get('ENCRYPTION_KEY');
    if (key === undefined) {
      throw new ConfigurationError(
        'ENCRYPTION_KEY required but not set. Must be set to public key PEM.',
      );
    }
    this.encryptionKey = createPublicKey({
      key,
      format: 'pem',
    });
  }

  private get verbose() {
    return ['1', 'true', 'yes'].includes(
      this.configService.get('VERBOSE', 'false'),
    );
  }

  private encryptEntry(data: string) {
    return encryptData(this.encryptionKey, data);
  }

  public async secureLogRequest(
    briefwahlFormDto: BriefwahlFormDto,
    timestamp?: string,
    remote_ip?: string,
  ): Promise<boolean> {
    try {
      const encryptedData = this.encryptEntry(
        JSON.stringify({ ...briefwahlFormDto, timestamp, remote_ip }),
      );
      await this.fileService.logEntry(
        Buffer.from(JSON.stringify({ encryptedData, timestamp })).toString(
          'base64',
        ),
      );
      this.verbose && console.log('BriefwahlService: entry logged');
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }

  public async sendMail(
    data: BriefwahlFormDto & {
      timestamp?: string;
      remote_ip?: string;
    },
  ): Promise<
    SMTPTransport.SentMessageInfo | BriefwahlServiceError | MailerServiceError
  > {
    const encrypt =
      ['true', 'yes'].includes(
        this.configService.get('ENCRYPT_BRIEFWAHL_MAIL', 'false'),
      ) && this.configService.get('ENCRYPTION_KEY') !== undefined;

    const subject = `[briefeintragung] ${
      this.configService.get('BRIEFWAHL_SUBJECT') ?? 'Briefeintragung'
    }`;

    const html = Object.entries(data)
      .map(([key, value]) => `<p>${key} = ${escape(value)}</p>`)
      .join('');

    let text = JSON.stringify(data);

    if (encrypt) {
      try {
        text = JSON.stringify({
          ...this.encryptEntry(text),
          timestamp: data.timestamp,
        });
      } catch (err) {
        console.error(err);
        return new BriefwahlServiceError(
          BriefwahlErrorType.EncryptionError,
          err,
        );
      }
    }

    const info = await this.mailerService.sendMail({
      to: this.configService.get('MAIL_TO_BRIEFWAHL'),
      from: this.configService.get('MAIL_FROM'),
      subject,
      text,
      html: encrypt ? undefined : html,
    });

    return info;
  }
}
