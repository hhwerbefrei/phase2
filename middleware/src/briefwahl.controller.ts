import {
  Body,
  Controller,
  Header,
  HttpStatus,
  Post,
  Redirect,
  Req,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { BriefwahlFormDto } from './messages/BriefwahlFormDto';
import { Request } from 'express';
import { BriefwahlService, isBriefwahlServiceError } from './briefwahl.service';
import { isMailerServiceError } from './mail/mailerservice';
import { to_single_string } from './util/util';

@Controller('briefeintragung')
export class BriefwahlController {
  constructor(private readonly service: BriefwahlService) {}

  @Post('send')
  @Redirect()
  @UsePipes(new ValidationPipe())
  @Header('Cache-Control', 'none')
  async submitBriefwahlForm(
    @Body() message: BriefwahlFormDto,
    @Req() request?: Request,
  ) {
    const timestamp = new Date().toISOString();

    const remote_ip =
      to_single_string(request?.headers['x-forwarded-for']) || 'unknown';

    const info = await this.service.sendMail({
      ...message,
      timestamp,
      remote_ip,
    });

    const logSuccess = await this.service.secureLogRequest(
      message,
      timestamp,
      remote_ip,
    );

    const redirect = request?.query['redirect'] || request?.headers.referer;

    const hasError =
      !logSuccess ||
      isBriefwahlServiceError(info) ||
      isMailerServiceError(info);

    if (redirect === undefined || typeof redirect !== 'string') {
      if (hasError) {
        return {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        };
      }
      return { statusCode: HttpStatus.ACCEPTED };
    }

    const redirectUri = decodeURI(redirect);
    const success = hasError
      ? 'false'
      : info.rejected?.length == 0
      ? 'true'
      : 'false';
    const successParam = `${
      /[?]/.test(redirectUri) ? '&' : '?'
    }success=${success}`;

    return {
      statusCode: HttpStatus.FOUND,
      url: `${redirectUri}${successParam}`,
    };
  }
}
