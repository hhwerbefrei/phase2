import {
  CipherKey,
  createCipheriv,
  createDecipheriv,
  KeyLike,
  KeyObject,
  privateDecrypt,
  publicEncrypt,
  randomBytes,
  RsaPrivateKey,
} from 'crypto';

export const to_b64 = (str: string) => Buffer.from(str).toString('base64');
export const from_b64 = (str: string) => Buffer.from(str, 'base64').toString();

function encryptText(text: string, key: CipherKey, iv: Buffer): Buffer {
  const cipher = createCipheriv('aes-128-cbc', key, iv);
  const encrypted = cipher.update(text);
  return Buffer.concat([encrypted, cipher.final()]);
}

export function encryptData(encryptionKey: KeyObject, data: string) {
  const encryptAESKey = (key: Buffer) => {
    const rsaEncryptedAesKey = publicEncrypt(encryptionKey, key);
    return rsaEncryptedAesKey;
  };

  const encryptPayload = (payload: string) => {
    const symmetricKey = randomBytes(16);
    const iv = randomBytes(16);

    const encryptedSymmetricKey = encryptAESKey(symmetricKey);
    const encryptedPayload = encryptText(payload, symmetricKey, iv);

    return {
      sk: encryptedSymmetricKey.toString('base64'),
      iv: iv.toString('base64'),
      payload: encryptedPayload.toString('base64'),
    };
  };

  return encryptPayload(data);
}

const decryptText = (
  ciphertext: Buffer,
  decryptionKey: CipherKey,
  iv: Buffer,
) => {
  const cipher = createDecipheriv('aes-128-cbc', decryptionKey, iv);
  const decrypted = cipher.update(ciphertext);
  return Buffer.concat([decrypted, cipher.final()]);
};

export function decryptData(
  decryptionKey: RsaPrivateKey | KeyLike,
  data: { sk: string; payload: string; iv: string },
): Buffer {
  const decryptedSymmetricKey = privateDecrypt(
    decryptionKey,
    Buffer.from(data.sk, 'base64'),
  );
  const decryptedPayload = decryptText(
    Buffer.from(data.payload, 'base64'),
    decryptedSymmetricKey,
    Buffer.from(data.iv, 'base64'),
  );
  return decryptedPayload;
}
