export const to_single_string = (arg: string | string[] | undefined) => {
  if (!arg) {
    return '';
  }
  if (typeof arg === 'string') {
    return arg;
  }
  return arg.join(',');
};
