import { Test } from '@nestjs/testing';
import { AppController } from './teilnahme.controller';
import { TeilnahmeService } from './teilnahme.service';
import { FormDto } from './messages/FormDto';
import { mockRequest } from 'mock-req-res';
import { HttpStatus } from '@nestjs/common';
import { MailerServiceError } from './mail/mailerservice';
import { mockMailInfo } from '../test/common';
import { SubscribeNewsletterDto } from '@/messages/SubscribeNewsletterDto';
import { ConfigModule, ConfigService } from '@nestjs/config';

describe('AppController', () => {
  const processNewsletterRequestMockFn = jest.fn(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async (formInput: FormDto) => true,
  );

  afterEach(jest.clearAllMocks);

  const getEnv = () => ({
    VERBOSE: 'true',
  });

  async function mkAppController(good: boolean) {
    const sendMockFn = jest
      .fn()
      .mockReturnValue(
        good
          ? mockMailInfo
          : new MailerServiceError(
              new Error('something went seriously wrong in the SMTP service'),
            ),
      );

    const app = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          load: [getEnv],
          envFilePath: [],
        }),
      ],
      controllers: [AppController],
      providers: [
        ConfigService,
        {
          provide: TeilnahmeService,
          useValue: {
            sendMail: sendMockFn,
            processNewsletterRequest: processNewsletterRequestMockFn,
          },
        },
      ],
    }).compile();

    return {
      app: app.get<AppController>(AppController),
      sendMock: sendMockFn,
    };
  }

  describe('handling of newsletter-only requests (subscribeNewsletter)', () => {
    const fieldInput: SubscribeNewsletterDto = {
      email: 'max@muster.muster',
    };

    const request = mockRequest({
      query: {},
    });

    it('should accept message and send newsletter request', async () => {
      const { app } = await mkAppController(true);

      const formInput = { ...fieldInput };

      expect(await app.subscribeNewsletter(formInput, request)).toEqual({
        statusCode: 202,
      });
      expect(processNewsletterRequestMockFn).toHaveBeenCalledWith(
        expect.objectContaining({
          ...formInput,
          newsletter: expect.stringMatching(/ja|true/),
        }),
      );
    });

    it('should accept message and sent newsletter request (with redirect)', async () => {
      const { app } = await mkAppController(true);

      const formInput = { ...fieldInput };

      const request = mockRequest({
        query: {
          redirect: 'foo',
        },
      });
      expect(await app.subscribeNewsletter(formInput, request)).toEqual({
        statusCode: 302,
        url: 'foo?success=true',
      });
      expect(processNewsletterRequestMockFn).toHaveBeenCalledWith(
        expect.objectContaining({
          ...formInput,
          newsletter: expect.stringMatching(/ja|true/),
        }),
      );
    });
  });

  describe('handling of requests', () => {
    const defaultFormInput: FormDto = {
      name: 'Max Musterfrau',
      anwerben: 'ja',
      erfahrung: 'ja',
      newsletter: 'false',
      email: 'max@muster.muster',
      nachricht: 'hey, ich finde eure kampagne toll!',
      phone: '0171-0000000',
      signal: 'ja',
      stadtteil: 'Barmbek',
      zeitumfang: '2h/Woche',
    };

    const request = mockRequest({
      query: {},
    });

    it('error case should return 500 if no redirect url can be found', async () => {
      const { app, sendMock } = await mkAppController(false);

      expect(await app.submitForm(defaultFormInput, request)).toEqual({
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      });
      expect(sendMock).toHaveBeenCalledWith(defaultFormInput);
    });

    it('should accept message if send succeeds', async () => {
      const { app, sendMock } = await mkAppController(true);

      expect(await app.submitForm(defaultFormInput, request)).toEqual({
        statusCode: HttpStatus.ACCEPTED,
      });
      expect(sendMock).toHaveBeenCalledWith(defaultFormInput);
      expect(processNewsletterRequestMockFn).toHaveBeenCalledWith(
        defaultFormInput,
      );
    });

    it('should redirect with success=false if send fails and redirect uri given', async () => {
      const { app, sendMock } = await mkAppController(false);

      const request = mockRequest({
        query: {
          redirect: 'foo',
        },
      });

      expect(await app.submitForm(defaultFormInput, request)).toEqual(
        expect.objectContaining({
          statusCode: HttpStatus.FOUND,
          url: expect.stringContaining('success=false'),
        }),
      );
      expect(sendMock).toHaveBeenCalledWith(defaultFormInput);
      expect(processNewsletterRequestMockFn).toHaveBeenCalledWith(
        defaultFormInput,
      );
    });

    it.each(['foo', 'https://example.com/foo?baz=bar'])(
      'should redirect if send succeeds and redirect uri given',
      async (redirect) => {
        const { app, sendMock } = await mkAppController(true);

        const request = mockRequest({
          query: {
            redirect,
          },
        });

        expect(await app.submitForm(defaultFormInput, request)).toEqual(
          expect.objectContaining({
            statusCode: HttpStatus.FOUND,
            url: expect.stringMatching(new RegExp('[&?]success=true$')),
          }),
        );
        expect(sendMock).toHaveBeenCalledWith(defaultFormInput);
        expect(processNewsletterRequestMockFn).toHaveBeenCalledWith(
          defaultFormInput,
        );
      },
    );

    it('should accept message and send newsletter request', async () => {
      const { app, sendMock } = await mkAppController(true);

      const formInput = { ...defaultFormInput, newsletter: 'ja' };

      expect(await app.submitForm(formInput, request)).toEqual({
        statusCode: 202,
      });
      expect(sendMock).toHaveBeenCalledWith(formInput);
      expect(processNewsletterRequestMockFn).toHaveBeenCalledWith(formInput);
    });
  });
});
