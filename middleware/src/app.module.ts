import { Module } from '@nestjs/common';
import { AppController } from './teilnahme.controller';
import { TeilnahmeService } from './teilnahme.service';
import { ConfigModule } from '@nestjs/config';
import envConfig from './configEnv';
import { MailerService } from './mail/mailerservice';
import { BriefwahlController } from './briefwahl.controller';
import { BriefwahlService } from './briefwahl.service';
import { FileService } from './fs/fileservice';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: envConfig.paths,
    }),
  ],
  controllers: [AppController, BriefwahlController],
  providers: [
    TeilnahmeService,
    BriefwahlService,
    MailerService,
    FileService,
    { provide: 'fs', useFactory: () => require('fs') },
    { provide: 'nodemailer', useFactory: () => require('nodemailer') },
  ],
})
export class AppModule {}
