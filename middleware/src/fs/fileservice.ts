import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
// see https://github.com/nestjs/nest/issues/191
import * as fs from 'fs';
type FsType = typeof fs;

@Injectable()
export class FileService {
  private briefwahlEintragLogFilePath: string;

  constructor(
    private readonly configService: ConfigService,
    @Inject('fs') private fs: FsType,
  ) {
    // debug: console.log(fs.readdirSync('/'));
    const briefwahlLogPath = this.configService.get('BRIEFWAHL_LOG_PATH');
    if (briefwahlLogPath) {
      this.briefwahlEintragLogFilePath = briefwahlLogPath;
    } else {
      console.error('logfile path missing, using default');
      this.briefwahlEintragLogFilePath = 'briefeintragung_log.txt';
    }
  }

  public async logEntry(data: string) {
    this.fs.appendFileSync(this.briefwahlEintragLogFilePath, data + '\n');
  }
}
