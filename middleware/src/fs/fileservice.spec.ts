import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { FileService } from './fileservice';

import { fs, vol } from 'memfs';
vol.fromJSON({ './.keep': '' }, '/foo/bar');

async function setupService() {
  const module = await Test.createTestingModule({
    imports: [ConfigModule.forRoot()],
    providers: [
      FileService,
      ConfigService,
      { provide: 'fs', useFactory: () => fs },
    ],
  }).compile();

  return {
    fileService: module.get<FileService>(FileService),
  };
}

describe('fileservice', () => {
  beforeAll(() => {
    process.env = {
      BRIEFWAHL_LOG_PATH: '/foo/bar/test_briefwahl_log.txt',
    };
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('logEntry', () => {
    it('works', async () => {
      const { fileService } = await setupService();

      const lines: string[] = [];

      for (let i = 0; i < 20; i++) {
        const line = `dies soll ein eintrag (${i}) sein`;
        lines.push(line);

        await fileService.logEntry(line);
      }

      expect(
        fs.readFileSync('/foo/bar/test_briefwahl_log.txt').toString(),
      ).toEqual(lines.join('\n') + '\n');
    });
  });
});
