# Konfiguration

Variablen für `.env` wie folgt:

```
MAIL_HOST='mail.app.example.com'
MAIL_USER='sender@app.example.com'
MAIL_FROM='sender@app.example.com'
MAIL_PASS='smtpPassword'
MAIL_TO='sammeln+phase2@example.com'
MAIL_TO_BRIEFWAHL='briefwahl@example.com'
BRIEFWAHL_LOG_PATH='briefeintragung_log.txt'
BRIEFWAHL_SUBJECT='Briefeintragung (Test)'
TEILNAHMEWUNSCH_SUBJECT='Teilnahmewunsch (Test)'
NEWSLETTER_ABONNIEREN_URL="https://api.example.com/mailing-list/subscribe/"
LISTEN_PORT=3002
ENCRYPTION_KEY='<public key pem>'
VERBOSE=true
ENCRYPT_BRIEFWAHL_MAIL=true
```

Default-Werte sind

```
LISTEN_PORT=3001
BRIEFWAHL_SUBJECT='Briefeintragung'
TEILNAHMEWUNSCH_SUBJECT='Teilnahmewunsch'
VERBOSE=false
ENCRYPT_BRIEFWAHL_MAIL=false
```
