export const mockMailInfo = {
  envelope: { from: '', to: [] },
  messageId: 'mymessageid',
  accepted: ['some@e.mail'],
  rejected: [],
  pending: [],
  response: 'responsefrommail',
};

export const mockMailInfo_bad = {
  envelope: { from: '', to: [] },
  messageId: 'mymessageid',
  accepted: [],
  rejected: ['foo@foo.foo'],
  pending: [],
  response: 'responsefrommail',
};
