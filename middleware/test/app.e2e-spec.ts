import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { MailerService } from '../src/mail/mailerservice';
import { ConfigModule } from '@nestjs/config';

import { fs, vol } from 'memfs';
import { decryptData } from '../src/util/crypto';
import { mockMailInfo } from './common';

vol.fromJSON({ './.keep': '' }, '/foo/bar');

const public_key =
  '-----BEGIN PUBLIC KEY-----\n\
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApGTZhkCz0RHESTaXmUfz\n\
iUh+WwjhYluPxdgCUpGMiS1e8Ew4u5oPCDE0kqNNFLspWTWwKErF933AnVEpCdmK\n\
SEMRCw4BasQ4XwmwrUikmtrxheTKNXXGdwO0OG3AIzhvVVlnyaiKx3VsJzwGyVKm\n\
kV+IQll+by0oVZTLgGC1CeHsYvpZsZk/Zg8MGXyNMz+Rb4Fnm6RcX4nV2kctTQI8\n\
9BC6NL/M0ix8spjvGl5Qib2ipuuB8N7qyfdKki56aPoMsLw1BdqpmHEf0UF6Qyzh\n\
9ZZraUtdDNXOr9CnzLFwoY6wNCAg87PtnQ2Vgg+bEXIInKSh/EqwgkDGzrBfopUQ\n\
VQIDAQAB\n\
-----END PUBLIC KEY-----';

const private_key =
  '-----BEGIN PRIVATE KEY-----\n\
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCkZNmGQLPREcRJ\n\
NpeZR/OJSH5bCOFiW4/F2AJSkYyJLV7wTDi7mg8IMTSSo00UuylZNbAoSsX3fcCd\n\
USkJ2YpIQxELDgFqxDhfCbCtSKSa2vGF5Mo1dcZ3A7Q4bcAjOG9VWWfJqIrHdWwn\n\
PAbJUqaRX4hCWX5vLShVlMuAYLUJ4exi+lmxmT9mDwwZfI0zP5FvgWebpFxfidXa\n\
Ry1NAjz0ELo0v8zSLHyymO8aXlCJvaKm64Hw3urJ90qSLnpo+gywvDUF2qmYcR/R\n\
QXpDLOH1lmtpS10M1c6v0KfMsXChjrA0ICDzs+2dDZWCD5sRcgicpKH8SrCCQMbO\n\
sF+ilRBVAgMBAAECggEALkLtDcEmeQaZaIry1WEwkOj0GdUha8bdaizz1l18IVxB\n\
s7iXXH+pjEIgi8VlmxhiNecMWAJWvGNIcVzWAh4UMmqhIK3Dy1JFlMUK0XC4VZWY\n\
UvgDyVCH1ZUWwaxs3dxzRB7hPJfv2dpa4Z2cQUo/cnhiRV2e3VVlXNP/AnT0bT+Q\n\
qrzPemE/gp/GPzxTvU0uuy0FG8RdOiFZ4BKoC1GE9SjZSYHNFhO0y3J0LyqtxgF8\n\
c44jl2OIY3Z4R07pqltLiMefYIJCyrn/vKJRz1rbyg2kuMVyVgSxje/vqnyTKR6v\n\
Ehdafy8HhBawQlj+A0rcpeu/qWwUaWI5uxnhhpVfzwKBgQDn/1WaSyxq8343curK\n\
qesIk9080imT0uoqiluPtouLP0RarSkNcKJdYR9NHoD39qlHrUPLWoufGs0iTIRf\n\
PuqrgqRuG40mX2f1IDKCK7+2DVYrylzUqfNk4VFNRwRik7z+c8niBJZefQRdQ7ox\n\
9po1JauRtWpeYphm550MJzelywKBgQC1Zvms6jjOM+5JQfkOOkdzieui6LgaS2OS\n\
R2WifPMT6yw+Lr4Opmm0gIHj3kW0PCrGFI4ir1nfEpNGToSUSTdruoK93rXS8QKi\n\
4pzgClBDKoF+BFuWUsEa2Yv/bB/sx1AVwMZzl03T0gr6TllK0Aa81bZH8bTFgoQj\n\
AJLEAGpeXwKBgQCJuaEd+eaij26siEIukfT/oJhEVRdttFZjbsOwa1QoFCUGF6o5\n\
WlZpTREB/Uve6MXR0WBfxp8Aky6YYAhtarJxoxruati+oQDcGoMhl8s1znELihTW\n\
AxaFGuXjFHfb9YzLGf4NeV+zNFB09ZIBuz4MsxJJbAN7iNikrncjEd0NRQKBgHF8\n\
+A0XB+zxjk1kqdUd3t/I1JO59pv0uFmms04hz82RXr+UbLErqYwUra6Ku58T2UrN\n\
R1Mp0xb9PwhITSecWsDaEbuJ3sUWpSiUj8KWkTpEGHlnbokwQsFyguSMl7iBxXem\n\
gEGG0lkH2nhYlgJ4/Q4cwgpmjErY6aQgUrGu9N0PAoGAZXm0Ll6aQuoB4P/5OKZm\n\
QjpH9iKzjZeKezvXfXCe9iU6Ud2RlCh01yGugJKLDrbUZHPcSqKTvsURbjjwHSDy\n\
WUFhXd0igNOf6mUGYuY1+DIE0TC2P3YTsw8enj1f4i+hC34uutFlEwIk4dp+aXlO\n\
Pb3MGgfb7Tl5R/LXjvyZ7ks=\n\
-----END PRIVATE KEY-----\
';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  const sendMockFn = jest.fn().mockReturnValue(mockMailInfo);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const subscribeMockFn = jest.fn(async (_email: string) => true);

  afterEach(jest.clearAllMocks);

  beforeEach(() => {
    jest.resetModules();
  });

  beforeEach(async () => {
    process.env = {
      VERBOSE: 'false',
      ENCRYPTION_KEY: public_key,
      BRIEFWAHL_LOG_PATH: '/foo/bar/test_briefwahl_log.txt',
      ENCRYPT_BRIEFWAHL_MAIL: 'false',
    };

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), AppModule],
      providers: [{ provide: 'fs', useFactory: () => fs }],
    })
      .overrideProvider(MailerService)
      .useValue({
        sendMail: sendMockFn,
        submitNewsletterRequest: subscribeMockFn,
      })
      .overrideProvider('fs')
      .useValue(fs)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('invalid route', () => {
    it('/no-such-route', async () => {
      await request(app.getHttpServer())
        .post('/no-such-route')
        .send({})
        .set('Content-type', 'application/json')
        .expect(404);
    });
  });

  describe('briefwahlformular', () => {
    describe('request with payload', () => {
      const payload = {
        vorname: 'Max',
        nachname: 'Musterfrau',
        beantragung: 'true',
        plz: '22527',
        strassenr: 'Vogt-Kölln-Str. 1024',
      };

      it('/submit (POST) with payload', async () => {
        await request(app.getHttpServer())
          .post('/briefeintragung/send')
          .send(payload)
          .set('Content-type', 'application/json')
          .expect(202);
        expect(sendMockFn).toHaveBeenCalledWith(
          expect.objectContaining({ html: expect.any(String) }),
        );

        const logFileContents = fs
          .readFileSync('/foo/bar/test_briefwahl_log.txt')
          .toString();
        const decoded = JSON.parse(
          Buffer.from(
            logFileContents.split('\n').at(0) ?? '',
            'base64',
          ).toString(),
        );
        const decrypted = decryptData(private_key, decoded.encryptedData);
        expect(JSON.parse(decrypted.toString())).toEqual(
          expect.objectContaining({
            ...payload,
            remote_ip: 'unknown',
            timestamp: expect.anything(),
          }),
        );
      });
    });
  });

  describe('Bloße Newsletter-Anmeldung', () => {
    describe('empty request', () => {
      it('/submit/subscribeNewsletter (POST), no redirect param', async () => {
        await request(app.getHttpServer())
          .post('/submit/subscribeNewsletter')
          .expect(400);
        expect(sendMockFn).not.toHaveBeenCalled();
      });
    });

    describe('request with payload', () => {
      const payload = {
        email: 'max@muster.muster',
      };

      it('/submit/subscribeNewsletter (POST), no redirect param', async () => {
        await request(app.getHttpServer())
          .post('/submit/subscribeNewsletter')
          .send(payload)
          .expect(202);
        expect(sendMockFn).not.toHaveBeenCalled();
        expect(subscribeMockFn).toHaveBeenCalledWith('max@muster.muster');
      });

      it('/submit/subscribeNewsletter (POST), redirect param', async () => {
        await request(app.getHttpServer())
          .post('/submit/subscribeNewsletter')
          .query({ redirect: 'foo' })
          .send(payload)
          .expect(302)
          .then((response) => {
            expect(response.body).toBeDefined();
            expect(response.headers?.location).toMatch('foo');
            expect(response.headers?.location).toMatch(/[?]success=true$/);
          });
        expect(sendMockFn).not.toHaveBeenCalled();
        expect(subscribeMockFn).toHaveBeenCalledWith('max@muster.muster');
      });
    });
  });

  describe('mitmachformular', () => {
    describe('empty request', () => {
      it('/submit (POST), no redirect param', async () => {
        await request(app.getHttpServer()).post('/submit/send').expect(400);
        expect(sendMockFn).not.toHaveBeenCalled();
      });

      it('/submit (POST), redirect param', async () => {
        await request(app.getHttpServer())
          .post('/submit/send')
          .query({ redirect: 'foo' })
          .expect(400)
          .then((response) => {
            expect(response.body).toBeDefined();
          });
        expect(sendMockFn).not.toHaveBeenCalled();
      });
    });

    describe('request with minimal payload', () => {
      const payload = {
        name: 'Max Musterfrau',
        email: 'max@muster.muster',
      };

      it('/submit (POST), no redirect param', async () => {
        await request(app.getHttpServer())
          .post('/submit/send')
          .send(payload)
          .expect(202);
        expect(sendMockFn).toHaveBeenCalledWith(
          expect.objectContaining({
            text: expect.stringMatching(/max@muster\.muster/),
          }),
        );
      });

      it('/submit (POST), redirect param', async () => {
        await request(app.getHttpServer())
          .post('/submit/send')
          .query({ redirect: 'foo' })
          .send(payload)
          .expect(302)
          .then((response) => {
            expect(response.body).toBeDefined();
            expect(response.headers?.location).toMatch('foo');
            expect(response.headers?.location).toMatch(/[?]success=true$/);
          });
        expect(sendMockFn).toHaveBeenCalled();
      });
    });

    describe('request with payload', () => {
      const payload = {
        name: 'Max Musterfrau',
        anwerben: 'ja',
        erfahrung: 'ja',
        email: 'max@muster.muster',
        nachricht: 'hey, ich finde eure kampagne toll!',
        phone: '0171-0000000',
        signal: 'ja',
        stadtteil: 'Barmbek',
        zeitumfang: '2h/Woche',
      };

      it('/submit (POST) with payload', async () => {
        await request(app.getHttpServer())
          .post('/submit/send')
          .send(payload)
          .set('Content-type', 'application/json')
          .expect(202);
        expect(sendMockFn).toHaveBeenCalledWith(
          expect.objectContaining({ html: expect.any(String) }),
        );
      });

      it.each(['nein', 'false', undefined])(
        '/submit (POST) with payload and redirect and no newsletter subscription (%s)',
        async (value) => {
          const redirectUri = 'https://base.base/';
          await request(app.getHttpServer())
            .post('/submit/send')
            .send({ ...payload, newsletter: value })
            .query({ redirect: encodeURI(redirectUri) })
            .set('Content-type', 'application/json')
            .expect(302)
            .then((response) => {
              expect(response.body).toBeDefined();
              expect(response.headers?.location).toMatch(redirectUri);
              expect(response.headers?.location).toMatch(/success=true$/);
            });
          expect(subscribeMockFn).not.toHaveBeenCalled();
          expect(sendMockFn).toHaveBeenCalled();
        },
      );

      it.each(['ja', 'true'])(
        '/submit (POST) with payload and redirect and with newsletter subscription (%s)',
        async (value) => {
          const redirectUri = 'https://base.base/';
          await request(app.getHttpServer())
            .post('/submit/send')
            .send({ ...payload, newsletter: value })
            .query({ redirect: encodeURI(redirectUri) })
            .set('Content-type', 'application/json')
            .expect(302)
            .then((response) => {
              expect(response.body).toBeDefined();
              expect(response.headers?.location).toMatch(redirectUri);
              expect(response.headers?.location).toMatch(/success=true$/);
            });
          expect(sendMockFn).toHaveBeenCalled();
          expect(subscribeMockFn).toHaveBeenCalledWith(payload.email);
        },
      );
    });
  });
});
